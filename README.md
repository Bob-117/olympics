# Atelier Big Data & IA

- [X] Gouriou Aurélien
- [X] Proust Stephen
- [X] Bricaud Olivier

## Sujet

### <ins>Cas pratique</ins> : Jeux Olympiques Paris 2024

Les JO se déroulent à Paris cette année. C’est l’occasion de s’intéresser à l’histoire des JO à travers ses données,
mais aussi de prédire les résultats aux JO de Paris.

1. Récupérer des datasets sur Kaggle et d’autres sites si besoin
2. A l’aide d’un notebook ou d’une webapp, analyser les jeux de données récupérés :
- Combien d’athlètes français ont été médaillés dans l’histoire des JO (120ans) ?
- Quels sont les plus grands athlètes français dans l’histoire des JO ?
- Comment se classe la France dans l’histoire des JO ?
- Quels sont les pays qui ont le plus gagné par discipline ?
- Quels sont les athlètes qui ont le plus gagné de médailles dans l’histoire des JO ?
3. Pouvez-vous prédire les pays médaillés dans au moins 2 disciplines lors des JO de Paris ?
4. Pouvez-vous prédire les athlètes médaillés dans au moins 2 disciplines lors des JO de Paris ?
5. Pouvez-vous prédire le nombre total de médailles françaises lors des JO de Paris ?
6. Que pensez-vous de cet article ?
7. Quelles conclusions tirez-vous de votre étude des JO par les données ? Pouvez-vous parier les yeux
fermés avec vos résultats ?
8. Avez-vous besoin du Big Data pour ce projet ? Justifiez votre réponse

### <ins>Technos</ins> :

- pandas (bob)
- yDATA (ste)
- matplotlib (bob)
- seaborn
- scikit-learn (bob)
- nicegui (orel)

## DATA

### <ins>Question 1</ins> : Récupération du dataset

Source principale de données : 
- https://www.kaggle.com/datasets/heesoo37/120-years-of-olympic-history-athletes-and-results?resource=download

Sources secondaires : 
- https://www.kaggle.com/code/joshuaswords/does-hosting-the-olympics-improve-performance (non traité)

Sources tiers :
- https://amybeisel.medium.com/machine-learning-for-the-olympics-4aa4188e6298
- https://github.com/AmyBeisel/Olympics-Data/blob/master/notebooks/LS_DS_231_assignment_AMY_BEISEL.ipynb
- https://gitlab.com/Bob-117/Gladiator/-/blob/master/src/algo/data.py?ref_type=heads

### <ins>Question 2</ins> : Analyse des données

- [X] Utilisation de ydata-profiling

```shell
python src/profiling.py --input data/athlete_events.csv --output data/athlete_events_report.html
```

![](readme/ydata_overview.png)
![](readme/ydata_variables.png)
![](readme/ydata_interactions.png)

- [X] Utilisation de pandas

```shell
python src/question_reponse.py
```
- Combien d’athlètes français ont été médaillés dans l’histoire des JO (120ans) ?

```txt
Question 1
Joueurs ayant une médaille :  39783
Joueurs médaillés français :  1550
Joueurs médaillés français en 2016 :  96
```

- Quels sont les plus grands athlètes français dans l’histoire des JO ?

```text
Question 2
Top 10 des joueurs français médaillés : 
Roger Franois Ducret                  8
Philippe Louis Eugne Cattiau          8
Lon Ernest Moreaux                    7
Lucien Alphonse Paul Gaudin           6
Christian Marie Auguste d'Oriola      6
Georges Eugne William "Go" Buchard    6
Maurice Marie Lecoq                   6
Daniel Jean Claude Ernest Revenu      6
Philippe Claude Riboud                6
Nicolas Achille Paroche               5
```

- Comment se classe la France dans l’histoire des JO ?

```text
Question 3
Top 10 des pays médaillés (nombre de médailles, MONDIAL) : 
0  United States  5219
1   Soviet Union  2451
2        Germany  1984
3  Great Britain  1673
4         France  1550
5          Italy  1527
6         Sweden  1434
7      Australia  1306
8         Canada  1243
9        Hungary  1127
Classement de la france en nombre de médailles:  5

Top 10 des pays médaillés (nombre de points, MONDIAL) : 
             Team  points
0  United States   18139
1   Soviet Union    8115
2        Germany    5954
3  Great Britain    4913
4          Italy    4683
5         France    4406
6         Sweden    4190
7         Canada    3757
8      Australia    3580
9        Hungary    3515
Classement de la france en nombre de points:  6

```
- Quels sont les pays qui ont le plus gagné par discipline ?
```text
Question 4
Top 3 des pays médaillés en  Tug-Of-War  : 
0  Great Britain
1         Sweden
2        Belgium

Top 3 des pays médaillés en  Swimming  : 
0  United States
1      Australia
2   East Germany

Top 3 des pays médaillés en  Ice Hockey  : 
0         Canada
1  United States
2         Sweden

Top 3 des pays médaillés en  Gymnastics  : 
0   Soviet Union
1  United States
2          Japan

Top 3 des pays médaillés en  Alpine Skiing  : 
0      Austria
1  Switzerland
2       France

Top 3 des pays médaillés en  Handball  : 
0   South Korea
1        Norway
2  Soviet Union

Top 3 des pays médaillés en  Hockey  : 
0  Netherlands
1    Australia
2      Germany
...
```
- Quels sont les athlètes qui ont le plus gagné de médailles dans l’histoire des JO ?
```text
Question 5
Top 20 des joueurs médaillés (historique MONDIAL) : 
 Name                                             Team          
Michael Fred Phelps, II                          United States     28
Larysa Semenivna Latynina (Diriy-)               Soviet Union      18
Nikolay Yefimovich Andrianov                     Soviet Union      15
Borys Anfiyanovych Shakhlin                      Soviet Union      13
Ole Einar Bjrndalen                              Norway            13
Takashi Ono                                      Japan             13
Edoardo Mangiarotti                              Italy             13
Dara Grace Torres (-Hoffman, -Minas)             United States     12
Ryan Steven Lochte                               United States     12
Sawao Kato                                       Japan             12
Paavo Johannes Nurmi                             Finland           12
Jennifer Elisabeth "Jenny" Thompson (-Cumpelik)  United States     12
Natalie Anne Coughlin (-Hall)                    United States     12
Aleksey Yuryevich Nemov                          Russia            12
Vra slavsk (-Odloilov)                           Czechoslovakia    11
Matthew Nicholas "Matt" Biondi                   United States     11
Mark Andrew Spitz                                United States     11
Viktor Ivanovych Chukarin                        Soviet Union      11
Carl Townsend Osburn                             United States     11
Aladr Gerevich (-Gerei)                          Hungary           10

Top 20 des joueurs médaillés (MONDIAL, sur les 10 dernières années) : 
 Name                                Team           Sport               
Michael Fred Phelps, II             United States  Swimming                12
Marit Bjrgen                        Norway         Cross Country Skiing     8
Aliya Farkhatovna Mustafina         Russia         Gymnastics               7
Nathan Ghar-Jun Adrian              United States  Swimming                 7
Allison Rodgers Schmitt             United States  Swimming                 7
Sun Yang                            China          Swimming                 6
Usain St. Leo Bolt                  Jamaica        Athletics                6
Irene Karlijn "Ireen" Wst           Netherlands    Speed Skating            6
Melissa Jeanette "Missy" Franklin   United States  Swimming                 6
Allyson Michelle Felix              United States  Athletics                6
Ryan Steven Lochte                  United States  Swimming                 6
Kathleen Genevieve "Katie" Ledecky  United States  Swimming                 6
Alexandra Rose "Aly" Raisman        United States  Gymnastics               6
Dana Whitney Vollmer (-Grant)       United States  Swimming                 6
Brittany Joyce Elmslie              Australia      Swimming                 5
Max Antony Whitlock                 Great Britain  Gymnastics               5
Simone Arianne Biles                United States  Gymnastics               5
Johan Arne Olsson                   Sweden         Cross Country Skiing     5
Shelly-Ann Fraser-Pryce             Jamaica        Athletics                5
Danuta Kozk                         Hungary        Canoeing                 5
```

- [X] Analyses complémentaires avec pandas (`src/data_extract.py`)

*Todo : Recupérer Outliers et Rescale (https://gitlab.com/Bob-117/Gladiator/ : src/algo/data.py, https://gitlab.com/Bob-117/nasa-data-management : base/core/transform.py)*

*Note : seul le fichier `athlete_events.csv` est utilisé, la conversion entre le `NOC` (National Olympic Committees) et le nom du pays peut se faire avec le fichier `noc_regions.csv` présent dans l'archive récupérée sur `kaggle`... et on s'en est pas rendu compte très tot...* 


<details>
<summary>Duplicated Regions</summary>

```shell
************** duplicated regions
+-----+-------+----------------+------------------------+
|     | NOC   | region         | notes                  |
|-----+-------+----------------+------------------------|
|   7 | ANZ   | Australia      | Australasia            |
|  12 | AUS   | Australia      | nan                    |
|  36 | CAN   | Canada         | nan                    |
| 147 | NFL   | Canada         | Newfoundland           |
|  41 | CHN   | China          | nan                    |
|  88 | HKG   | China          | Hong Kong              |
|  54 | CZE   | Czech Republic | nan                    |
| 197 | TCH   | Czech Republic | nan                    |
|  26 | BOH   | Czech Republic | Bohemia                |
|  70 | FRG   | Germany        | nan                    |
|  76 | GDR   | Germany        | nan                    |
|  79 | GER   | Germany        | nan                    |
| 173 | SAA   | Germany        | nan                    |
|  51 | CRT   | Greece         | Crete                  |
|  81 | GRE   | Greece         | nan                    |
| 124 | MAL   | Malaysia       | nan                    |
| 126 | MAS   | Malaysia       | nan                    |
| 143 | NBO   | Malaysia       | North Borneo           |
| 214 | URS   | Russia         | nan                    |
| 171 | RUS   | Russia         | nan                    |
|  66 | EUN   | Russia         | nan                    |
| 175 | SCG   | Serbia         | Serbia and Montenegro  |
| 185 | SRB   | Serbia         | nan                    |
| 227 | YUG   | Serbia         | Yugoslavia             |
| 195 | SYR   | Syria          | nan                    |
| 210 | UAR   | Syria          | United Arab Republic   |
| 205 | TTO   | Trinidad       | Trinidad and Tobago    |
| 223 | WIF   | Trinidad       | West Indies Federation |
| 220 | VIE   | Vietnam        | nan                    |
| 222 | VNM   | Vietnam        | nan                    |
| 225 | YEM   | Yemen          | nan                    |
| 224 | YAR   | Yemen          | North Yemen            |
| 226 | YMD   | Yemen          | South Yemen            |
| 167 | RHO   | Zimbabwe       | nan                    |
| 229 | ZIM   | Zimbabwe       | nan                    |
| 168 | ROT   | nan            | Refugee Olympic Team   |
| 208 | TUV   | nan            | Tuvalu                 |
| 213 | UNK   | nan            | Unknown                |
+-----+-------+----------------+------------------------+
```
</details>


```shell
Top 20 NOC (occurrence) : ['USA', 'GBR', 'FRA', 'ITA', 'GER', 'CAN', 'SWE', 'JPN', 'AUS', 'HUN', 'POL', 'NED', 'SUI', 'URS', 'FIN', 'ESP', 'CHN', 'RUS', 'AUT', 'NOR']
```

```shell
***** STATS *****
271116 Rows, 15 Cols (['ID', 'Name', 'Sex', 'Age', 'Height', 'Weight', 'Team', 'NOC', 'Games', 'Year', 'Season', 'City', 'Sport', 'Event', 'Medal']
************** Olympics dataset sample
+----+------+--------------------------+-------+-------+----------+----------+----------------+-------+-------------+--------+----------+-------------+---------------+------------------------------------+---------+
|    |   ID | Name                     | Sex   |   Age |   Height |   Weight | Team           | NOC   | Games       |   Year | Season   | City        | Sport         | Event                              | Medal   |
|----+------+--------------------------+-------+-------+----------+----------+----------------+-------+-------------+--------+----------+-------------+---------------+------------------------------------+---------|
|  0 |    1 | A Dijiang                | M     |    24 |      180 |       80 | China          | CHN   | 1992 Summer |   1992 | Summer   | Barcelona   | Basketball    | Basketball Men's Basketball        | nan     |
|  1 |    2 | A Lamusi                 | M     |    23 |      170 |       60 | China          | CHN   | 2012 Summer |   2012 | Summer   | London      | Judo          | Judo Men's Extra-Lightweight       | nan     |
|  2 |    3 | Gunnar Nielsen Aaby      | M     |    24 |      nan |      nan | Denmark        | DEN   | 1920 Summer |   1920 | Summer   | Antwerpen   | Football      | Football Men's Football            | nan     |
|  3 |    4 | Edgar Lindenau Aabye     | M     |    34 |      nan |      nan | Denmark/Sweden | DEN   | 1900 Summer |   1900 | Summer   | Paris       | Tug-Of-War    | Tug-Of-War Men's Tug-Of-War        | Gold    |
|  4 |    5 | Christine Jacoba Aaftink | F     |    21 |      185 |       82 | Netherlands    | NED   | 1988 Winter |   1988 | Winter   | Calgary     | Speed Skating | Speed Skating Women's 500 metres   | nan     |
|  5 |    5 | Christine Jacoba Aaftink | F     |    21 |      185 |       82 | Netherlands    | NED   | 1988 Winter |   1988 | Winter   | Calgary     | Speed Skating | Speed Skating Women's 1,000 metres | nan     |
|  6 |    5 | Christine Jacoba Aaftink | F     |    25 |      185 |       82 | Netherlands    | NED   | 1992 Winter |   1992 | Winter   | Albertville | Speed Skating | Speed Skating Women's 500 metres   | nan     |
|  7 |    5 | Christine Jacoba Aaftink | F     |    25 |      185 |       82 | Netherlands    | NED   | 1992 Winter |   1992 | Winter   | Albertville | Speed Skating | Speed Skating Women's 1,000 metres | nan     |
|  8 |    5 | Christine Jacoba Aaftink | F     |    27 |      185 |       82 | Netherlands    | NED   | 1994 Winter |   1994 | Winter   | Lillehammer | Speed Skating | Speed Skating Women's 500 metres   | nan     |
|  9 |    5 | Christine Jacoba Aaftink | F     |    27 |      185 |       82 | Netherlands    | NED   | 1994 Winter |   1994 | Winter   | Lillehammer | Speed Skating | Speed Skating Women's 1,000 metres | nan     |
+----+------+--------------------------+-------+-------+----------+----------+----------------+-------+-------------+--------+----------+-------------+---------------+------------------------------------+---------+
----- Nb col by type -----
object : 15
----- Cols name by type -----
object : ['ID', 'Name', 'Sex', 'Age', 'Height', 'Weight', 'Team', 'NOC', 'Games', 'Year', 'Season', 'City', 'Sport', 'Event', 'Medal']
----- Unique values for object type column (limit 20) -----
ID : [135571]
Name : [134732]
Sex : [['M', 'F']]
Age : [75]
Height : [96]
Weight : [221]
Team : [1184]
NOC : [230]
Games : [51]
Year : [35]
Season : [['Summer', 'Winter']]
City : [42]
Sport : [66]
Event : [765]
Medal : [[nan, 'Gold', 'Bronze', 'Silver']]
Ignored col for unique values : []
```

```shell
************** Nb <NaN> for each :
+--------+-----------------+
|        |   missing_count |
|--------+-----------------|
| ID     |               0 |
| Name   |               0 |
| Sex    |               0 |
| Age    |            9474 |
| Height |           60171 |
| Weight |           62875 |
| Team   |               0 |
| NOC    |               0 |
| Games  |               0 |
| Year   |               0 |
+--------+-----------------+
Col with <NaN> : ['Age', 'Height', 'Weight', 'Medal']
************** NaN handling : 
Result for col Age : 9474 items
Rows with <NaN> in cols <Age> are deleted
Result for col Height : 51574 items
Result for col Weight : 54263 items
Result for col Medal : 222591 items
9474 rows deleted (271116 => 261642)
Col ['Height', 'Weight', 'Medal'] : handling NaN not implemented
```


```shell
************** Duplicated NOC for regions (38 rows | 3 cols)
+-----+-------+----------------+------------------------+
|     | NOC   | region         | notes                  |
|-----+-------+----------------+------------------------|
|   7 | ANZ   | Australia      | Australasia            |
|  12 | AUS   | Australia      | nan                    |
|  36 | CAN   | Canada         | nan                    |
| 147 | NFL   | Canada         | Newfoundland           |
|  41 | CHN   | China          | nan                    |
|  88 | HKG   | China          | Hong Kong              |
...
Top 20 NOC (occurrence) : ['USA', 'GBR', 'FRA', 'ITA', 'GER', 'CAN', 'SWE', 'JPN', 'AUS', 'HUN', 'POL', 'NED', 'SUI', 'URS', 'FIN', 'ESP', 'CHN', 'RUS', 'AUT', 'NOR']
```


- [X] Visualisation avec matplotlib et seaborn (`src/data_visu.py`)

Exploration de la feature `taille de la délégation olympique` (calculée à partir du nombre d'athlètes par commité olympique par évènement (saison + année)). 
Cette feature pourra être intéressante dans l'entraînement de nos modèles prédictifs pour la suite des questions Machine Learning. 
Cependant l'analyse et la visualisation de nos données est plus vaste qu'une unique feature, ici ne sont seulement présentés que quelques résultats donnés à titre d'exemple.

| #                | Summer 1936                                                                                                                      |     |
|------------------|----------------------------------------------------------------------------------------------------------------------------------|-----|
| pie %            | ![](readme/Winter_2014/delegation_size_winter_2014_pie_percentage.png)                                                           |     |
| pie %            | ![](readme/Winter_2014/delegation_size_summer_1936_pie_percentage.png)                                                           |     |
| *notes*          | Répartition de la taille de délégation en pourcentage                                                                            |     |
| pie count        | ![](readme/Summer_1936/delegation_size_summer_1936_pie_count.png)                                                                |     |
| *notes*          | Répartition de la taille de délégation en nombre d'individus                                                                     |     |
| hist avg         | ![](readme/Summer_1936/delegation_size_summer_1936_hist_avg.png)                                                                 |     |
| *notes*          | Non trié, comparison a la moyenne                                                                                                |     |
| hist quartile    | ![](readme/Summer_1936/delegation_size_summer_1936_hist_quartile.png)                                                            |     |
| *notes*          | Comparaison aux quartiles, sur `Winter 2014` on obtient `Nb values over q1 & q3 : 21 high, 42 mid, 26 low`                       |     |
| qq               | ![](readme/Summer_1936/delegation_size_summer_1936_qq.png)                                                                       |     |
| *notes*          | https://en.wikipedia.org/wiki/Q%E2%80%93Q_plot<br> On test une distribution normale, ca colle plus ou moins                      |     |
| sns distribution | ![](readme/Summer_1936/delegation_size_summer_1936_sns_distribution.png)                                                         |     |
| *notes*          | Utilisation de seaborn                                                                                                           |     |
| hist fitted      | ![](readme/Summer_1936/delegation_size_summer_1936_hist.png) ![](readme/Summer_1936/delegation_size_summer_1936_hist_fitted.png) |     |
| *notes*          | Et c'est beau                                                                                                                    |     |


## MACHINE LEARNING (`src/machine_learning.py`)

### <ins>Question 3</ins> : Pouvez-vous prédire les pays médaillés dans au moins 2 disciplines lors des JO de Paris ?

- Modèle : SVM
- Features : len(delegation), count(all medals until today), bool(same_continent)
- Target : bool(more than 1 medal in 2 sports)
- Notes : 
  - Nos individus d'étude sont composés d'un évènement (season + year) et d'un pays associé

#### Selection des features

- [X] Dataset initial (traité et propre, les `nan` de la colonne `Medal` sont remplacés pas une valeur par défault)

```shell
Raw information for svm (261642 rows | 7 cols | truncated)
+----+-------+--------+----------+-------------+---------+---------------+--------------------------+
|    | NOC   |   Year | Season   | City        | Medal   | Sport         | Name                     |
|----+-------+--------+----------+-------------+---------+---------------+--------------------------|
|  0 | CHN   |   1992 | Summer   | Barcelona   | nope    | Basketball    | A Dijiang                |
|  1 | CHN   |   2012 | Summer   | London      | nope    | Judo          | A Lamusi                 |
|  2 | DEN   |   1920 | Summer   | Antwerpen   | nope    | Football      | Gunnar Nielsen Aaby      |
|  3 | DEN   |   1900 | Summer   | Paris       | Gold    | Tug-Of-War    | Edgar Lindenau Aabye     |
|  4 | NED   |   1988 | Winter   | Calgary     | nope    | Speed Skating | Christine Jacoba Aaftink |
+----+-------+--------+----------+-------------+---------+---------------+--------------------------+
```

- [X] Traitement des médailles (One Hot Encode)

```shell
One Hot Encode
************** Encoded medals (261642 rows | 10 cols | truncated)
+----+-------+--------+----------+-------------+---------------+--------------------------+----------------+--------------+----------------+--------------+
|    | NOC   |   Year | Season   | City        | Sport         | Name                     |   Medal_Bronze |   Medal_Gold |   Medal_Silver |   Medal_nope |
|----+-------+--------+----------+-------------+---------------+--------------------------+----------------+--------------+----------------+--------------|
|  0 | CHN   |   1992 | Summer   | Barcelona   | Basketball    | A Dijiang                |              0 |            0 |              0 |            1 |
|  1 | CHN   |   2012 | Summer   | London      | Judo          | A Lamusi                 |              0 |            0 |              0 |            1 |
|  2 | DEN   |   1920 | Summer   | Antwerpen   | Football      | Gunnar Nielsen Aaby      |              0 |            0 |              0 |            1 |
|  3 | DEN   |   1900 | Summer   | Paris       | Tug-Of-War    | Edgar Lindenau Aabye     |              0 |            1 |              0 |            0 |
|  4 | NED   |   1988 | Winter   | Calgary     | Speed Skating | Christine Jacoba Aaftink |              0 |            0 |              0 |            1 |
|  5 | NED   |   1988 | Winter   | Calgary     | Speed Skating | Christine Jacoba Aaftink |              0 |            0 |              0 |            1 |
```

- [X] Traitement des évènements (season + year + noc)

```py
specific_event = features_df[(features_df['NOC'] == country) & (features_df['Year'] == year) & (features_df['Season'] == season)]
```

```shell
************** FRA Summer 1992 (486 rows | 10 cols | truncated)
+------+-------+--------+----------+-----------+-----------------------+-------------------------------+----------------+--------------+----------------+--------------+
|      | NOC   |   Year | Season   | City      | Sport                 | Name                          |   Medal_Bronze |   Medal_Gold |   Medal_Silver |   Medal_nope |
|------+-------+--------+----------+-----------+-----------------------+-------------------------------+----------------+--------------+----------------+--------------|
| 1574 | FRA   |   1992 | Summer   | Barcelona | Sailing               | Yannick Adde                  |              0 |            0 |              0 |            1 |
| 1712 | FRA   |   1992 | Summer   | Barcelona | Canoeing              | Franck Adisson                |              1 |            0 |              0 |            0 |
| 1919 | FRA   |   1992 | Summer   | Barcelona | Synchronized Swimming | Marianne Aeschbacher          |              0 |            0 |              0 |            1 |
| 1920 | FRA   |   1992 | Summer   | Barcelona | Synchronized Swimming | Marianne Aeschbacher          |              0 |            0 |              0 |            1 |
| 2330 | FRA   |   1992 | Summer   | Barcelona | Canoeing              | Marianne Agulhon              |              0 |            0 |              0 |            1 |
| 4799 | FRA   |   1992 | Summer   | Barcelona | Water Polo            | Thierry Gille Alimondo        |              0 |            0 |              0 |            1 |
| 5895 | FRA   |   1992 | Summer   | Barcelona | Shooting              | Jean-Pierre Bernard Andr Amat |              0 |            0 |              0 |            1 |
| 5896 | FRA   |   1992 | Summer   | Barcelona | Shooting              | Jean-Pierre Bernard Andr Amat |              0 |            0 |              0 |            1 |
| 5897 | FRA   |   1992 | Summer   | Barcelona | Shooting              | Jean-Pierre Bernard Andr Amat |              0 |            0 |              0 |            1 |
| 7502 | FRA   |   1992 | Summer   | Barcelona | Volleyball            | Rivomanantsoa Andriamaonju    |              0 |            0 |              0 |            1 |
+------+-------+--------+----------+-----------+-----------------------+-------------------------------+----------------+--------------+----------------+--------------+
```

- [X] Feature `same_continent` (bool)

```python
def is_in_continent(_noc_code, _city=''):
    for _continent, _noc in noc_continent.items():
        if _noc_code in _noc and _city in cities_continent.get(_continent, []):
            if country == 'FRA':
                print(f'{_noc_code} and {_city} are in {_continent}')
            return True
        if country == 'FRA':
            print(f'{_noc_code} and {_city} are not both in {_continent}')
    return False
```

```shell
# Comparaison entre le continent du NOC et le continent de la ville de déroulement des jeux olympiques, le calcul des distances ou le temps de trajet pourra être implémenté ultérieurement
FRA and Barcelona are not both in Africa
FRA and Barcelona are not both in Asia
FRA and Barcelona are in Europe
```

- [X] Feature `taille de la délégation` et `nombre total de médailles jusqu'à l'évènement présent` : simples calculs de `len()` et de somme itérative sur les évènements.

```python
delegation = len(specific_event['Name'].unique())
```

```python
total_medals_in_history += medal_this_event
```

- [X] Target : bool(1 médaille dans 2 sports différents)

```shell
************** FRA Summer 1992 medals_count_by_sport (12 rows | 3 cols | truncated)
+---------------+----------------+----------------+--------------+
| Sport         |   Medal_Bronze |   Medal_Silver |   Medal_Gold |
|---------------+----------------+----------------+--------------|
| Archery       |              0 |              0 |            1 |
| Athletics     |              0 |              0 |            1 |
| Canoeing      |              5 |              1 |            0 |
| Cycling       |              4 |              1 |            0 |
| Equestrianism |              4 |              0 |            0 |
| Fencing       |              7 |              0 |            2 |
| Handball      |             16 |              0 |            0 |
| Judo          |              4 |              1 |            2 |
| Sailing       |              0 |              0 |            3 |
| Shooting      |              0 |              1 |            0 |
+---------------+----------------+----------------+--------------+
There are at least two different sports with medals FRA Summer 1992 : ['Canoeing' 'Shooting' 'Equestrianism' 'Cycling' 'Swimming' 'Judo'
 'Fencing' 'Sailing' 'Handball' 'Archery' 'Table Tennis' 'Athletics']
```

```python
two_medal_target = len(sports_with_medals) >= 2
```

- [X] Dataset final

````shell
+----+-----------------------+------------------+-------------------+-------------------------+--------------------------------+
|    | has_won_more_that_2   | same_continent   |   delegation_size |   total_medal_til_today | metadata                       |
|----+-----------------------+------------------+-------------------+-------------------------+--------------------------------|
|  0 | True                  | True             |                31 |                      33 | FIN-Winter-2014-Sochi          |
|  1 | True                  | False            |                 7 |                      40 | FIN-Winter-1948-Sankt Moritz   |
|  2 | True                  | True             |                11 |                      57 | FIN-Summer-1948-London         |
|  3 | True                  | True             |                 9 |                      69 | FIN-Winter-1952-Oslo           |
|  4 | True                  | True             |                26 |                      97 | FIN-Summer-1952-Helsinki       |
|  5 | True                  | True             |                10 |                     110 | FIN-Winter-1992-Albertville    |
|  6 | True                  | True             |                 7 |                     117 | FIN-Summer-1992-Barcelona      |
|  7 | True                  | True             |                27 |                     147 | FIN-Winter-1994-Lillehammer    |
|  8 | True                  | False            |                 9 |                     160 | FIN-Winter-2002-Salt Lake City |
|  9 | True                  | True             |                40 |                     201 | FIN-Winter-2006-Torino         |
+----+-----------------------+------------------+-------------------+-------------------------+--------------------------------+
````

- [X] Entrainement & Validation (croisée ici): 

```shell
# ==================================
Dataset size : 3804
# ==================================
Accuracy: 0.8804204993429697
Confusion Matrix:
[[499  37]
 [ 54 171]]
# ==================================
Classification Report:
              precision    recall  f1-score   support

       False       0.90      0.93      0.92       536
        True       0.82      0.76      0.79       225

    accuracy                           0.88       761
   macro avg       0.86      0.85      0.85       761
weighted avg       0.88      0.88      0.88       761
# ==================================
Cross-Validation Scores: [0.90968801 0.89819376 0.90804598 0.89144737 0.92598684]
Mean Accuracy: 0.906672392187365
Standard Deviation: 0.011727891785022156
# ==================================
                 Feature  Importance
0         same_continent    0.261299
1        delegation_size    0.037883
2  total_medal_til_today    0.001053
# ==================================
```

![](readme/svm_confusion.png)

- [X] Prediction :

```sh
Input : {'same_continent': True, 'delegation_size': 50, 'total_medal_til_today': 0} 
Output : Will win at least 1 medal in two separate sports : [ False ]

Input : {'same_continent': True, 'delegation_size': 100, 'total_medal_til_today': 100}
Output : Will win at least 1 medal in two separate sports : [ True ]

Input : {'same_continent': True, 'delegation_size': 10000, 'total_medal_til_today': 0}
Output : Will win at least 1 medal in two separate sports : [ True ]

Input : {'same_continent': True, 'delegation_size': 500, 'total_medal_til_today': 100}
Output : Will win at least 1 medal in two separate sports : [ True ]

Input : {'same_continent': False, 'delegation_size': 100, 'total_medal_til_today': 0}
Output : Will win at least 1 medal in two separate sports : [ False ]

Input : {'same_continent': False, 'delegation_size': 1000, 'total_medal_til_today': 0}
Output : Will win at least 1 medal in two separate sports : [ True ]

Input : {'same_continent': False, 'delegation_size': 100, 'total_medal_til_today': 100000}
Output : Will win at least 1 medal in two separate sports : [ True ]
```


### <ins>Question 4</ins> : Pouvez-vous prédire les athlètes médaillés dans au moins 2 disciplines lors des JO de Paris ?

- Modèle : KNN (`src/knn.py`)
- Features : ['in_continent', 'weight', 'summer', 'age', 'height']
- Target : 1 medal in at least 2 disciplines
- Notes :
  - nos individus d'étude sont les athlètes pour un évènement donné

- [X] La selection des features et le calcul de la target se font avec les meme traitements pandas sur des informations spécifiques et choisies 
 

L'étude sur différents nombres de voisins sur les features [`in_continent`, `weight`, `summer`, `age`, `height`] avec pour target `two_medal` (calculée) : 

![](readme/knn_roc_18800.png)

![](readme/knn_roc_145244.png)

*Il faut environ 50 minutes pour traiter l'ensemble du dataset d'origine, rien n'est optimisé ou refacto en l'état*

Étant donné l'aspect général des courbes et les valeurs des aires sous la courbe, nous pouvons supposer que le choix du modèle et/ou le choix des features n'est pas en accord avec la problématique de départ. Il faut revoir les données et l'entrainement. 


- [X] Prédictions

````shell
Prediction for KNN | 2 neighbors
Input : athlete {'in_continent': True, 'height': 150, 'weight': 70, 'age': 70, 'summer': 70}
Output : will win 1 medal in at least 2 disciplines [False]

Prediction for KNN | 3 neighbors
Input : athlete {'in_continent': False, 'height': 150, 'weight': 70, 'age': 70, 'summer': 70}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': False, 'height': 180, 'weight': 80, 'age': 80, 'summer': 80}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': False, 'height': 210, 'weight': 100, 'age': 100, 'summer': 100}
Output : will win 1 medal in at least 2 disciplines [False]

Prediction for KNN | 5 neighbors
Input : athlete {'in_continent': True, 'height': 150, 'weight': 70, 'age': 70, 'summer': 70}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': True, 'height': 180, 'weight': 80, 'age': 80, 'summer': 80}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': True, 'height': 210, 'weight': 100, 'age': 100, 'summer': 100}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': False, 'height': 150, 'weight': 70, 'age': 70, 'summer': 70}
Output : will win 1 medal in at least 2 disciplines [False]

Prediction for KNN | 8 neighbors
Input : athlete {'in_continent': True, 'height': 150, 'weight': 70, 'age': 70, 'summer': 70}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': True, 'height': 180, 'weight': 80, 'age': 80, 'summer': 80}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': True, 'height': 210, 'weight': 100, 'age': 100, 'summer': 100}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': False, 'height': 150, 'weight': 70, 'age': 70, 'summer': 70}
Output : will win 1 medal in at least 2 disciplines [False]
Input : athlete {'in_continent': False, 'height': 180, 'weight': 80, 'age': 80, 'summer': 80}
Output : will win 1 medal in at least 2 disciplines [False]
````

### <ins>Question 5</ins> : Pouvez-vous prédire le nombre total de médailles françaises lors des JO de Paris ?

- Modèle : Linear Regression ? Neural Network ?

- Idée 1 : (cas Fr indépendant du reste, (x1, x2,..., xn) = (année, nb medailles, nombre d'inscrits en club de sport, budget du gouvernenemt, autres event sport dans l'année (bool ou count)))
- Idée 2 : Dépendance entre les pays, les pays concurrents (de meme envergure) ==> comment ?

### <ins>Question 6</ins> : Que pensez-vous de cet [article](https://arxiv.org/abs/2012.04378) ?

L'article explique techniquement le processus de recherche autour d'un modèle de Random Forest en deux étapes, en effectuant des comparaisons
avec d'autres modèles, et en expliquant quelles sont les features les plus importantes pour le modèle. 

Il y est également décrit comment l'équipe à orienter ses recherches pour atteindre les meilleures prédictions possibles.

Mis à part toutes ces explications techniques, l'article traite aussi des intérêts potentiels d'un modèle de ce genre, en citant notamment les paris sportifs, les médias, les sponsors.
Il y est aussi expliqué comment le modèle pourrait être utilisé par les gouvernements pour évaluer l'investissement qu'ils doivent faire dans le sport.

Il est important de mesurer l'impact que peut avoir un tel modèle, car s'il est techniquement très intéressant de se pencher sur de telles prédictions, il est très facile de dévier de ses intentions et de se retrouver à utiliser ce modèle pour des fins plus ou moins éthiques. 

Les sponsors pourraient s'en servir pour favoriser un athlète plus qu'un autre. Une société de paris sportifs pourrait s'en servir pour privilégier les paris sur un athlète plutôt qu'un autre.

En bref, toutes les décisions qui pourraient être prises à partir de ce modèle doivent être prises avec précaution, et il est important de garder en tête que ce modèle n'est qu'un outil, et qu'il ne doit pas être utilisé pour prendre des décisions à la place de l'humain.
Les avancées globales dans l'IA ces dernières années posent une réelle question de cadre, ou de règlement pour encadrer l'utilisation de ces technologies.

Il parait essentiel qu'un cadre clair, une charte éthique, bref, que des limites soient posées pour préserver les décisions humaines et les chances de tous.


### <ins>Question 7</ins> : Quelles conclusions tirez-vous de votre étude des JO par les données ? Pouvez-vous parier les yeux fermés avec vos résultats ?

Non, du machine learning en 2 jours, ou même 2 semaines, je ne fais pas confiance les yeux fermés :^)

### <ins>Question 8</ins> : Avez-vous besoin du Big Data pour ce projet ? Justifiez votre réponse

Le traitement des données peut être fastidieux (*eg : for year in dataset, for season in year, for country, count medal*) et l'utilisation des techos performantes est un élément à prendre en compte dans l'implémentation de ce projet.
Les 98 456 `Names` ou les 200k+ lignes de datasets présentent deja des dimensions importantes et l'utilisation de batch (lots) peut s'avérer utile dans le traitement, le calcul et l'enregistrement des valeurs et features. 
50 minutes


### NiceGUI

## NiceGUI
![nicegui](orel/assets/nicegui.png)

Expérimentations avec NiceGUI, l'objectif est d'afficher les datas, de pouvoir filtrer dessus dans le front ainsi que de pouvoir générer des visualisations intéressantes.

### Run NiceGUI :
```
pip install nicegui
cd orel/front
python main.py
```
Puis, allez sur http://localhost:5001/olympic/stats
-> Redirection vers `/auth/login`, les identifiants sont :
- Nom : admin | Mot de passe : admin
- Nom : user | Mot de passe : password

-> Redirection vers `olympic/stats` et affichage

En haut de la page se situe un graphique qui résume toutes les médailles gagnées par les équipes françaises, classées par année. Il est possible de filtrer les types de médailles affichées.
![graphique](orel/assets/image.png)
_Nombre de médailles françaises par année et type_

En dessous se situe un tableau et trois sélecteurs qui permettent de filtrer les données affichées selon trois critères :
- Le pays
- L'année
- Le type de médaille

On peut ensuite lancer la requête, et le tableau affiche directement les datas filtrées depuis le CSV.
Une pagination est également mise en place pour une meilleure lisibilité.
![tableau](orel/assets/tableau.png)
_Tableau avec la requête 2012, Italie, Or._

La page de login est récupérée des exemples fournis par Gaël DURAND, notre formateur, lors de l'atelier.

![login](orel/assets/login.png)
_Page de login_

### Améliorations possibles :
- Ajouter des graphiques plus intéressants, et modulables grâce aux filtres du tableau
- Ajouter d'autres pages
- Rendre le front générique pour pouvoir afficher n'importe quel CSV


