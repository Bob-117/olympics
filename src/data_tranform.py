import numpy as np
import pandas as pd
from data_extract import __advanced_stats, print_dataset


# **********************************************************************************************************************
# Nan
# **********************************************************************************************************************
def identify_nan_values(_df):
    nan_values = _df.isna().sum()
    nan_values_df = pd.DataFrame(_df.isna().sum(), columns=['missing_count'])
    print_dataset(nan_values_df, title='Nb <NaN> for each : ', _sample=False)
    nan_col = list(
        filter(
            lambda key: nan_values[key] > 0,
            nan_values.keys().tolist()
        )
    )
    # print(f'Col with <NaN> : {nan_col}')
    return nan_col


def identify_nan_in_specific_column(_df, _col):
    """
    for c in identify_nan_values(_df):
        identify_nan_in_specific_column(c)
    """
    _ids = _df[_df[_col].isnull()].index.tolist()
    return _ids


def replace_nan_by_mean_in_specific_column(_df, col_name):
    col_mean = _df[col_name].mean()
    print(f'<NaN> in <{col_name}> are replaced by col mean ( {col_mean} )')
    _df[col_name] = _df[col_name].fillna(col_mean)
    return _df


def replace_nan_by_value_in_specific_column(_df, col_name, v):
    print(f'<NaN> in <{col_name}> are replaced by a specific value ({v})')
    # _df[col_name] = _df[col_name].fillna(v)
    _df.loc[:, col_name] = _df[col_name].fillna(v)
    return _df


def remove_nan_rows(_df, col_name):
    print(f'Rows with <NaN> in cols <{col_name}> are deleted')
    _df = _df.dropna(subset=[col_name])
    return _df


def nan_process(_df):
    """
    NaN process
    :param _df:
    :return:
    """
    print('*******************************')
    print('************** NaN ************')
    print('*******************************')
    # Init
    rows_before = len(_df)
    not_implemented = []

    # Find NaN
    nan_col = identify_nan_values(_df)

    # NaN handling
    print('************** NaN handling : ')
    for c in _df.columns:
        idx = identify_nan_in_specific_column(_df, c)
        if len(idx) > 0:
            print(f'Result for col {c} : {idx if len(idx) < 20 else f"{len(idx)} items"}')
        # Handle NaN
        if c == 'replace_nan_by_mean':
            _df = replace_nan_by_mean_in_specific_column(_df=_df, col_name=c)
        elif c in ['Age', 'Height', 'Weight']:
            _df = remove_nan_rows(_df=_df, col_name=c)
        elif c == 'Medal':
            _df = replace_nan_by_value_in_specific_column(_df=_df, col_name=c, v='nope')
        else:
            if c in nan_col:
                not_implemented.append(c)
    # remove_nan_rows(_df=df, col_name=nan_col)
    rows_after = len(_df)
    print(f'{rows_before - rows_after} rows deleted ({rows_before} => {rows_after})')
    print(f'Col {not_implemented} : handling NaN not implemented')
    return _df


# **********************************************************************************************************************
#  One Hot Encode
# **********************************************************************************************************************
def one_hot_encode(dataframe):
    """
    One Hot Encode
    :param dataframe:
    :return:
    """
    print('************** One Hot Encode process')
    before = len(dataframe.columns)
    columns_to_encode = __col_to_encode(dataframe)
    ohe = len(columns_to_encode)
    encoded_dataframe = pd.get_dummies(dataframe, columns=columns_to_encode, dtype=int)
    after = len(encoded_dataframe.columns)
    print(f'one_hot_encode | before {before} + {ohe} | after : {after} ({encoded_dataframe.columns}')
    return encoded_dataframe


def __col_to_encode(_dataframe):
    # dimension and memory purpose
    _max = 6

    _col_to_encode = []
    d = __advanced_stats(_dataframe=_dataframe)
    unique_values_dict = d.get('unique')
    for col_name, uniques in unique_values_dict.items():
        unique_values = uniques[0]
        # logging.debug(f'{uniques} VS {unique_values} - {type(unique_values)}')
        # print(f'{uniques} VS {unique_values} - {type(unique_values)}')
        if type(unique_values) in [np.ndarray, list]:
            if 1 < len(unique_values) <= _max:
                _col_to_encode.append(col_name)
    print(f'Col to One Hot Encode ({len(_col_to_encode)} with less than max {_max} unique values) : {_col_to_encode}')
    return _col_to_encode
