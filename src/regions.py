import pandas as pd

from data_extract import print_dataset, duplicated_values


########################################################################################################################
# METHODS
########################################################################################################################
def duplicated_regions(regions):
    print('duplicated')
    duplicated_regions_name = duplicated_values(regions, col='region')
    print_dataset(duplicated_regions_name, title='Duplicated NOC for regions', _sample=False)


def top_regions(_dataframe, top=20):
    top_20_noc = pd.DataFrame(_dataframe['NOC'].value_counts().head(top))
    top_20_noc_values = top_20_noc.index
    print(f'Top {top} NOC (occurrence) : {list(top_20_noc_values)}')


# def handle_regions(athletes, regions):
#     df_merged = athletes.merge(regions, on='NOC', how='left')
#     print_dataset(df_merged, title='Join on NOC with regions')
#     return df_merged


########################################################################################################################
# EXECUTION
########################################################################################################################
def regions_process(athletes, regions):
    duplicated_regions(regions=regions)
    top_regions(athletes)
    # new_regions = handle_regions(athletes, regions)
    # print(len(new_regions))
    # valid = new_regions[(new_regions['region'] == 'Australia')]
    # print_dataset(valid)
    # exit(117)

noc_continent = {
    "Africa": ["DJI", "EGY", "ALG", "CMR", "SEN", "NGR", "SUD", "MAR", "TUN", "KEN", "GUI", "RSA", "LIB", "UGA", "ETH",
               "GHA", "COD", "ZIM", "ANG", "CIV", "TAN", "LES", "SWZ", "SLE", "MLI", "NAM", "BDI", "SOM", "COM", "STP",
               "GAB", "GMB", "CPV", "BFA", "MTN", "SEY", "ERI"],
    "Asia": ["HKG", "KOR", "CHN", "KSA", "AZE", "KAZ", "PAK", "TJK", "UZB", "KGZ", "GEO", "INA", "BRN", "BHU", "TLS",
             "JOR", "TKM", "COM", "STP", "PLW", "IVB", "BDI", "HAI", "CGO", "SAM", "MAL", "ASA", "MDV", "GUM", "AHO",
             "SSD", "IOA"],
    "Europe": ["POL", "FRA", "EST", "AUT", "SWE", "RUS", "FRG", "UKR", "CYP", "HUN", "SUI", "TCH", "ITA", "LUX", "DEN",
               "TUR", "FIN", "CZE", "GER", "SLO", "BEL", "SCG", "BUL", "NED", "SVK", "NOR", "BLR", "POR", "LAT", "VIE",
               "IRL", "LTU", "SMR", "MNE", "CRO", "ISR", "DOM", "TTO", "MDA", "BIH", "VEN", "SRB", "CRC", "BER", "LIE",
               "MON", "ISL", "AND", "GIB", "MLT", "MKD", "COK"],
    "North America": ["PUR", "USA", "CAN", "MEX", "CUB", "BAH", "JAM", "DOM", "TTO", "CRC", "BER", "GUM", "AHO", "ESA",
                      "PAN"],
    "Oceania": ["AUS", "NZL", "FIJ", "ASA", "GUM", "AHO", "COK"],
    "South America": ["ARG", "BRA", "COL", "PER", "ECU", "VEN", "URU", "CHI", "PAR", "BOL", "GUY", "SUR"]
}

cities_continent = {
    "Asia": ["Seoul", "Tokyo", "Nagano", "Sapporo", "Beijing"],
    "Europe": ["Paris", "Antwerpen", "Athina", "Cortina d'Ampezzo", "Helsinki", "Stockholm", "Oslo", "Berlin",
               "Sarajevo", "Roma", "Grenoble", "Innsbruck", "Amsterdam", "Lillehammer", "Moscow", "Sochi", "Munich",
               "Barcelona", "London", "St. Moritz", "Garmisch-Partenkirchen", "Albertville"],
    "North America": ["Montreal", "St. Louis", "Mexico City", "Calgary", "Los Angeles", "Lake Placid", "Atlanta",
                      "Salt Lake City", "Vancouver"],
    "Oceania": ["Melbourne", "Sydney"]
}


def is_in_continent(_city, _noc_code):
    for _continent, noc in noc_continent.items():
        if _noc_code in noc and _city in cities_continent.get(_continent, []):
            print(f'{noc_code} and {city} are in {_continent}')
            return True
        print(f'{noc_code} and {city} are not both in {_continent}')
    return False


if __name__ == '__main__':
    cities_to_check = ['London', 'Melbourne', 'Paris']
    noc_code = 'FRA'

    for city in cities_to_check:
        result = is_in_continent(city, noc_code)
        # print(f"Is {city} in Europe with NOC code {noc_code}? {result}")

