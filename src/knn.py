import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve, auc
from sklearn.neighbors import KNeighborsClassifier

from src.data_extract import print_dataset, unique_values_by_col
from src.data_tranform import nan_process
from src.machine_learning import prepare_data

noc_continent = {
    "Africa": ["DJI", "EGY", "ALG", "CMR", "SEN", "NGR", "SUD", "MAR", "TUN", "KEN", "GUI", "RSA", "LIB", "UGA", "ETH",
               "GHA", "COD", "ZIM", "ANG", "CIV", "TAN", "LES", "SWZ", "SLE", "MLI", "NAM", "BDI", "SOM", "COM", "STP",
               "GAB", "GMB", "CPV", "BFA", "MTN", "SEY", "ERI"],
    "Asia": ["HKG", "KOR", "CHN", "KSA", "AZE", "KAZ", "PAK", "TJK", "UZB", "KGZ", "GEO", "INA", "BRN", "BHU", "TLS",
             "JOR", "TKM", "COM", "STP", "PLW", "IVB", "BDI", "HAI", "CGO", "SAM", "MAL", "ASA", "MDV", "GUM", "AHO",
             "SSD", "IOA", "CHA", "EUN", "YAR", "YMD"],
    "Europe": ["POL", "FRA", "EST", "AUT", "SWE", "RUS", "FRG", "UKR", "CYP", "HUN", "SUI", "TCH", "ITA", "LUX", "DEN",
               "TUR", "FIN", "CZE", "GER", "SLO", "BEL", "SCG", "BUL", "NED", "SVK", "NOR", "BLR", "POR", "LAT", "VIE",
               "IRL", "LTU", "SMR", "MNE", "CRO", "ISR", "DOM", "TTO", "MDA", "BIH", "VEN", "SRB", "CRC", "BER", "LIE",
               "MON", "ISL", "AND", "GIB", "MLT", "MKD", "COK", "GDR", "WIF"],
    "North America": ["PUR", "USA", "CAN", "MEX", "CUB", "BAH", "JAM", "DOM", "TTO", "CRC", "BER", "GUM", "AHO", "ESA",
                      "PAN", "NBO", "ARU", "NRU"],
    "Oceania": ["AUS", "NZL", "FIJ", "ASA", "GUM", "AHO", "COK"],
    "South America": ["ARG", "BRA", "COL", "PER", "ECU", "VEN", "URU", "CHI", "PAR", "BOL", "GUY", "SUR"]
}

cities_continent = {
    "Asia": ["Seoul", "Tokyo", "Nagano", "Sapporo", "Beijing"],
    "Europe": ["Paris", "Antwerpen", "Athina", "Cortina d'Ampezzo", "Helsinki", "Stockholm", "Oslo", "Berlin",
               "Sarajevo", "Roma", "Grenoble", "Innsbruck", "Amsterdam", "Lillehammer", "Moscow", "Sochi", "Munich",
               "Barcelona", "London", "St. Moritz", "Garmisch-Partenkirchen", "Albertville", "Torino", "Chamonix"],
    "North America": ["Montreal", "St. Louis", "Mexico City", "Calgary", "Los Angeles", "Lake Placid", "Atlanta",
                      "Salt Lake City", "Vancouver", "Squaw Valley"],
    "Oceania": ["Melbourne", "Sydney"]
}


def knn_athlete_two_medals(data):
    """
     Pouvez-vous prédire les athlètes médaillés dans au moins 2 disciplines lors des JO de Paris ?
    :return:
    """
    features_df = __knn_features_selection(df=data)
    __knn__process(features_df)


def __knn_features_selection(df):
    import time

    start_time = time.time()

    print(f'Feature selection for KNN in {df.columns}')
    athletes_informations = df[
        ['Name', 'Medal', 'Height', 'Weight', 'Season', 'Year', 'NOC', 'City', 'Games', 'Event', 'Sport', 'Age']]
    columns_to_encode = ['Medal', 'Season']
    athletes_informations = pd.get_dummies(athletes_informations, columns=columns_to_encode, dtype=int)

    # print(print_dataset(athletes_informations))
    # athletes_informations['concatenated'] = pd.concat(
    #     [
    #         athletes_informations['Season'].astype(str),
    #         athletes_informations['Year'].astype(str)
    #     ], axis=1).agg('-'.join, axis=1)
    #
    # athletes_informations = athletes_informations.drop(['Season', 'Year'], axis=1)
    # print_dataset(athletes_informations)
    athletes_names = athletes_informations['Name'].unique()
    out = []
    N = len(athletes_names)
    aaa = 0
    for i, n in enumerate(athletes_names):

        print(f'{n} - {i} / {N}')
        athletes_informations = athletes_informations.copy()
        current_athlete = athletes_informations[athletes_informations['Name'] == n]

        """
        FEATURE :  Basic Age, Height, Weight
        Average if more than one unqiue value ?
        """
        weight, height, age = current_athlete['Weight'].unique(), current_athlete['Height'].unique(), current_athlete[
            'Age'].unique()

        def remove_this_method(v):
            if len(v) > 1:
                v = sum(v) / len(v)
            else:
                v = v[0]
            return v

        weight, height, age = remove_this_method(weight), remove_this_method(height), remove_this_method(age)

        # print(weight, height, age)

        # first_year, last_year = current_athlete['Year'].unique()

        for game in current_athlete['Games'].unique():
            print(f'============ game {game} {aaa} {time.time() - start_time:.2f}')
            current_year = current_athlete[(current_athlete['Games'] == game)]

            # for discipline in current_athlete['Event'].unique():
            #     print(discipline)
            # print_dataset(current_year, title='current year')

            # print(type(current_year))
            # print(current_year.columns)
            #
            # print(current_year['Year'], type(current_year['Year']))
            # print(current_year['Year'].unique())
            #
            # print(current_year['NOC'], type(current_year['NOC']))
            # print(current_year['NOC'].unique())

            # print('====================================================')
            # unique_values = {
            #     f'{c}': [
            #         list((current_year[c].unique())) if len(current_year[c].unique()) < 50 else len(
            #             current_year[c].unique())]
            #     for c in current_year.columns
            # }
            # print('=======> ', unique_values)

            # print('====================================================')
            # print_dataset(sports_with_medals, title=f'{n} - {year} - sports_with_medals')
            # first_year, last_year = current_athlete['Year'].unique()
            # print(first_year)

            # if len(current_year) > 1:
            """
            Target : at least 1 medal in 2 different sports 
            """
            sports_with_medals = current_year[
                current_year[['Medal_Bronze', 'Medal_Silver', 'Medal_Gold']].sum(axis=1) > 0]['Event'].unique()

            two_medal_target = len(sports_with_medals) >= 2
            # if two_medal_target:
            #     print(f'===> {n} - {i} / {N}', sports_with_medals)

            """
            FEATURE :  same continent or not (replace by distance ?)
            """
            # print(current_year)
            current_city = list(current_year['City'].unique())
            # print(current_city)
            current_country = current_year['NOC'].unique()

            # print(current_country)

            def is_in_continent(_noc_code, _city=''):
                if type(_noc_code) is list:
                    _noc_code = _noc_code[0]
                if type(_city) is list:
                    _city = _city[0]

                try:
                    for _continent, _noc in noc_continent.items():
                        if _noc_code in _noc and _city in cities_continent.get(_continent, []):
                            # if current_country == 'FRA':
                            #     print(f'{_noc_code} and {_city} are in {_continent}')
                            return True
                        # if current_country == 'FRA':
                        #     print(f'{_noc_code} and {_city} are not both in {_continent}')
                    return False
                except:
                    return False

            in_continent = is_in_continent(current_country, current_city[0])

            """
            FEATURE :  Season
            """
            is_summer = 'Summer' in game

            """
            RESULT
            """
            res = {
                'two_medal': two_medal_target,
                'in_continent': in_continent,
                'age': age,
                'height': height,
                'weight': weight,
                'summer': is_summer
            }
            aaa += 1

            out.append(res)
    ml_dataset = pd.DataFrame(out)
    print_dataset(ml_dataset, title='Final Dataset for KNN')
    ml_dataset.to_csv('knn_dataset_all.csv', index=False)
    return ml_dataset


def __knn__process(_data):
    print('============================================================')
    print('KNN PROCESS')
    print('============================================================')
    print(_data.columns)
    print_dataset(_data, title='KNN FEATURES IN', _sample=True, nb=20)
    features = ['in_continent', 'weight', 'summer', 'age', 'height']
    AAA = len(_data)
    x_train, x_test, y_train, y_test = prepare_data(_df=_data, _features=features, _target='two_medal')

    print(x_train)
    print(x_test)
    print(y_train)
    print(y_test)

    neighbors_list = [2, 3, 5, 8]
    plt.figure(figsize=(10, 6))
    for n_neighbors in neighbors_list:
        print(f'Neighbors : {n_neighbors}')
        knn_classifier = KNeighborsClassifier(n_neighbors=n_neighbors)
        knn_classifier.fit(x_train, y_train)
        y_probs = knn_classifier.predict_proba(x_test)[:, 1]
        fpr, tpr, thresholds = roc_curve(y_test, y_probs)
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, label=f'k={n_neighbors}, AUC = {roc_auc:.2f}')

        new_data = [
            [True, 150, 70, 18, True],
            [True, 180, 80, 25, True],
            [True, 210, 100, 34, False],
            [False, 150, 70, 34, True],
            [False, 180, 80, 18, False],
            [False, 210, 100, 25, False],
        ]

        for data in new_data:
            new = {
                'in_continent': data[0],
                'height': data[1],
                'weight': data[2],
                'age': data[2],
                'summer': data[2],
            }
            new_data = np.array([[new['in_continent'], new['height'], new['weight'], new['age'], new['summer']]])

            prediction = knn_classifier.predict(new_data)
            print(f'Prediction for KNN | {n_neighbors} neighbors')
            print(f'Input : athlete {new}')
            print(f'Output : will win 1 medal in at least 2 disciplines {prediction}')
    plt.plot([0, 1], [0, 1], linestyle='--', color='gray', label='random classifier')

    plt.title(f'ROC Curves for KNN (k={neighbors_list}) with {AAA} entries in dataset')
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend()

    plt.show()


def process():
    data = pd.read_csv('../data/athlete_events.csv')
    print(len(data))
    data = nan_process(data)
    print(len(data))
    knn_athlete_two_medals(data)


if __name__ == '__main__':
    process()
