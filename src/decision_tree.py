import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier, plot_tree


def decision_tree_classifier(_df, target_col):

    target_column = 'Battle Experience'

    X = _df.drop(columns=[c for c in _df.columns if _df[c].dtype not in ['int64', 'float64']])
    X = X.drop('Battle Experience', axis=1)
    y = _df[target_column].astype(str)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    model = DecisionTreeClassifier()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print(f"Accuracy: {accuracy:.2f}")
    print("Classification Report:")
    print(classification_report(y_test, y_pred))

    feature_importances = model.feature_importances_
    feature_importance_df = pd.DataFrame({'Feature': X.columns, 'Importance': feature_importances})
    feature_importance_df = feature_importance_df.sort_values(by='Importance', ascending=False)

    print("Feature Importances:")
    print(feature_importance_df)

    # Plot feature importances
    plt.figure(figsize=(10, 6))
    plt.bar(feature_importance_df['Feature'], feature_importance_df['Importance'])
    plt.xlabel('Feature')
    plt.ylabel('Importance')
    plt.title(f'Feature Weight for feature {target_column}')
    plt.xticks(rotation=45, ha='right')
    plt.show()

    plt.figure(figsize=(20, 10))
    plot_tree(model, feature_names=X.columns, class_names=model.classes_.astype(str).tolist(),
              filled=True, rounded=True, max_depth=3, fontsize=10)
    plt.show()


if __name__ == '__main__':
    data = pd.read_csv('../data/athlete_50_random.csv')
    decision_tree_classifier(data, target_col='huho')