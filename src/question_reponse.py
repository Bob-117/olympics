import logging
import os

import pandas as pd
from pandas import DataFrame


def filter_teams(df, teams):
    """
    Filter the dataframe by teams
    :param df: dataframe
    :param teams: list of teams
    :return: filtered dataframe
    """
    return df[df['Team'].isin(teams)]


def filter_sports(df, sports):
    """
    Filter the dataframe by sports
    :param df: dataframe
    :param sports: list of sports
    :return: filtered dataframe
    """
    return df[df['Sport'].isin(sports)]


def filter_medals(df, medals):
    """
    Filter the dataframe by medals
    :param df: dataframe
    :param medals: list of medals
    :return: filtered dataframe
    """
    return df[df['Medal'].isin(medals)]


def filter_year(df, year):
    """
    Filter the dataframe by year
    :param df: dataframe
    :param year: year
    :return: filtered dataframe
    """
    return df[df['Year'] == year]


def question_1(df):
    """
    Combien d’athlètes français ont été médaillés dans l’histoire des JO (120ans) ?
    :param df:
    :return:
    """
    print("Question 1")
    print("Joueurs ayant une médaille : ", with_medals.shape[0])
    french = with_medals.loc[with_medals['Team'] == 'France']
    print("Joueurs médaillés français : ", french.shape[0])
    year_2016 = french.loc[french['Year'] == 2016]
    print("Joueurs médaillés français en 2016 : ", year_2016.shape[0])


def question_2(df):
    """
    Quels sont les (10) plus grands athlètes français dans l’histoire des JO ?
    :param df:
    :return:
    """
    print("\n\nQuestion 2")
    top_10_french = filter_teams(df, ["France"]).groupby(['ID', 'Name']).size().sort_values(
        ascending=False).head(50)
    print("Top 10 des joueurs français médaillés : \n", top_10_french)


def question_3(df):
    """
    Comment se classe la France dans l’histoire des JO ?
    (considérons 5points pour une médaille d’or, 3 pour une médaille d’argent, 1 pour une médaille de bronze)
    :param df:
    :return:
    """
    print("\n\nQuestion 3")
    print("Nombre de médailles : ", df.shape[0])
    print("distribution : ", df.value_counts(['Medal']))
    best_world = df.groupby('Team').size().sort_values(ascending=False)
    # add an index to the dataframe on display
    print("Top 10 des pays médaillés (nombre de médailles, MONDIAL) : \n", best_world.reset_index().head(10))
    print("Classement de la france en nombre de médailles: ",
          best_world.reset_index()[best_world.reset_index()['Team'] == 'France'].index[0] + 1)
    print("Maintenant, on va faire un classement en fonction du nombre de points (5/3/1)")
    new_df = df.copy()
    new_df['points'] = new_df['Medal'].apply(lambda x: 5 if x == 'Gold' else (3 if x == 'Silver' else 1))
    # group by team and sum the points
    new_df = new_df.groupby('Team').sum().sort_values(by='points', ascending=False)
    # show Team, points
    new_df = new_df.reset_index()[['Team', 'points']]
    print("Top 10 des pays médaillés (nombre de points, MONDIAL) : \n", new_df.head(10))
    print("Classement de la france en nombre de points: ", new_df[new_df['Team'] == 'France'].index[0] + 1)


def question_4(df):
    """
    Quels sont les pays qui ont le plus gagné __par discipline__ ?
    :param df:
    :return:
    """
    # for each sport, get the top 10 teams
    print("\n\nQuestion 4")
    disciplines = df['Sport'].unique().tolist()
    for sport in disciplines:
        top_3_sport = df[df['Sport'] == sport].groupby('Team').size().sort_values(
            ascending=False).reset_index().head(3)
        # if there are less than 4 teams, add empty rows
        if top_3_sport.shape[0] < 3:
            logging.warning("There are less than 3 teams for sport " + sport)
            continue
        print("Top 3 des pays médaillés en ", sport, " : \n",
              DataFrame(top_3_sport, columns=['Team']))


def question_5(df):
    """
    Quels sont les athlètes qui ont le plus gagné de médailles dans l’histoire des JO ?
    :param df:
    :return:
    """
    print("\n\nQuestion 5")
    top_20_world = df.groupby(['ID', 'Name', 'Team']).size().sort_values(
        ascending=False).head(20)
    print("\nTop 20 des joueurs médaillés (historique MONDIAL) : \n",
          top_20_world)
    print("\n\nTop 20 des joueurs médaillés (MONDIAL, sur les 10 dernières années) : \n",
          df[df['Year'] >= 2010].groupby(['ID', 'Name', 'Team', 'Sport']).size().sort_values(
              ascending=False).head(20))


if __name__ == '__main__':
    PROJECT_DIR = os.path.dirname(os.path.abspath(__file__)).split('src')[0]
    __df = pd.read_csv(os.path.join(PROJECT_DIR, 'data', 'athlete_events.csv'))
    with_medals = __df.dropna(subset=['Medal'])

    question_1(with_medals.copy())
    question_2(with_medals.copy())
    question_3(with_medals.copy())
    question_4(with_medals.copy())
    question_5(with_medals.copy())
