import os
import statistics
import seaborn as sns
import scipy.stats as stats

from matplotlib import pyplot as plt
from scipy.stats import norm
import numpy as np

from data_extract import print_dataset

SAVEFIG_DIR = '/home/bob/EPSI_I1/olympics/readme'


# years = ['1936' '1948' '1956' '1960' '1964' '1968' '1972' '1980' '1988' '1996'
#          '2004' '2008' '2012' '2016' '1952' '1976' '1984' '1992' '2000' '2006'
#          '2010' '2014' '1994' '1998' '2002' '1908' '1912' '1900' '1920' '1924'
#          '1928' '1932' '1896' '1904' '1906']


def delegation(df):
    df = df[['NOC', 'Year', 'Season']]
    print_dataset(df)
    delegation_count_df = df.groupby(['NOC', 'Year', 'Season']).size().reset_index(name='delegation_size')
    return delegation_count_df


def delegation_specific_event():
    ...


def plot(df, save=False):
    for y, s in [
        ('1936', 'Summer'),
        # ('2014', 'Winter')
    ]:
        __plot(df=df, year=y, season=s, _save=save)


def __plot(df, year, season, _save=False):
    if _save:
        png_dir = f'{SAVEFIG_DIR}/{season}_{year}/'
        if not os.path.isdir(png_dir):
            os.makedirs(png_dir)

    # delegation size
    delegation_count_df = delegation(df)
    print_dataset(delegation_count_df, title='Delegation count')
    print(delegation_count_df['Year'].unique())

    # validation
    # noc, year = 'AFG', '1936'
    # v = delegation_count_df[(delegation_count_df['NOC'] == noc) & (delegation_count_df['Year'] == year)]
    # valid = df[(df['NOC'] == noc) & (df['Year'] == year)]
    # print(f'Expected : {len(valid)}\nActual : \n{v}')

    # pie %
    # print_dataset(delegation_count_df, title='delegation count')
    delegation_count_specific_event = delegation_count_df[
        (delegation_count_df['Year'] == year) & (delegation_count_df['Season'] == season)]
    sorted_delegation = delegation_count_specific_event.sort_values('delegation_size', ascending=False)
    print_dataset(delegation_count_specific_event, title='summer 36')
    print_dataset(sorted_delegation, title=f'{season} {year} sorted')
    plt.figure(figsize=(8, 8))
    plt.pie(sorted_delegation['delegation_size'], labels=sorted_delegation['NOC'], autopct='%1.1f%%')
    plt.title(f'Delegation Size by National Olympic Committees  for {season} {year} (%)')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_percentage.png')
    else:
        plt.show()

    # pie count
    def pie_percentage_format(values):
        def my_format(pct):
            total = sum(values)
            v = int(round(pct * total / 100.0))
            return f'{v:d}'

        return my_format

    plt.figure(figsize=(8, 8))
    plt.pie(sorted_delegation['delegation_size'], labels=sorted_delegation['NOC'],
            autopct=pie_percentage_format(sorted_delegation['delegation_size']))
    plt.title(f'Delegation Size by National Olympic Committees for {season} {year} (count)')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_percentage.png')
    else:
        plt.show()

    # hist
    hist_avg(delegation_count_specific_event, season=season, year=year, _save=_save)
    hist_quartile(sorted_delegation, season=season, year=year, _save=_save)

    # Gauss
    qq_plot(sorted_delegation, season=season, year=year, _save=_save)
    sns_gauss(sorted_delegation, season=season, year=year, _save=_save)
    gauss(sorted_delegation, season=season, year=year, _save=_save)


def hist_avg(df, year, season, _save=False):
    k, v = df['NOC'], df['delegation_size']
    avg = statistics.mean(v)
    print(f'AVG : {avg}')
    plt.figure(2)
    colors = ['green' if x > avg else 'blue' for x in v]
    plt.bar(k, v, color=colors)
    plt.axhline(y=avg, color='r', linestyle='-')
    plt.xlabel('National Olympic Committees')
    plt.xticks([])
    plt.ylabel('Delegation size')
    plt.title(f'Delegation size | Average {avg:.1f} | {season} {year}')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_hist_avg.png')
    else:
        plt.show()


def hist_quartile(df, year, season, _save=False):
    k, v = df['NOC'], df['delegation_size']
    q1 = statistics.quantiles(v, n=4)[0]
    q3 = statistics.quantiles(v, n=4)[-1]
    colors = ['green' if x > q3 else 'blue' if x > q1 else 'red' for x in v]
    count_high = len(list(filter(lambda x: x == 'green', colors)))
    count_mid = len(list(filter(lambda x: x == 'blue', colors)))
    count_low = len(list(filter(lambda x: x == 'red', colors)))
    print(f'Nb values over q1 & q3 : {count_high} high | {count_mid} mid | {count_low} low')
    plt.figure(3)
    plt.bar(k, v, color=colors)
    plt.axhline(y=q1, color='r', linestyle='--')
    plt.axhline(y=q3, color='g', linestyle='--')
    plt.xlabel('National Olympic Committees')
    plt.xticks([])
    plt.ylabel('Delegation size')
    plt.title(f'Delegation size | Quartile ({q1} | {q3}) | {season} {year}')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_hist_quartile.png')
    else:
        plt.show()


def gauss(_df, year, season, _col='delegation_size', _save=False):
    # _df["Year"] = pd.to_numeric(_df["Year"])
    # sorted_data = np.sort(_df)
    # mu, std = norm.fit(sorted_data)
    # plt.hist(sorted_data, bins=25, density=True, alpha=0.6, color='g')
    # xmin, xmax = plt.xlim()
    # x = np.linspace(xmin, xmax, 100)
    # p = norm.pdf(x, mu, std)
    # plt.plot(x, p, 'k', linewidth=2)
    # title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
    # plt.title(title)
    # plt.show()

    ###########

    # gaussian_index = np.argsort(delegation_count_specific_event['delegation_size'])
    # print(gaussian_index)
    # delegation_count_specific_event['GaussianIndex'] = gaussian_index
    # print(delegation_count_specific_event)
    # huho = delegation_count_specific_event.sort_values(by='GaussianIndex')
    # print(huho)
    # hist_avg(huho)

    # hist
    plt.hist(_df[_col], bins=20, facecolor='blue', alpha=0.5)
    plt.title('Histogram of Delegation size')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_hist.png')
    else:
        plt.show()

    # hist
    x = _df[_col]
    mean = np.mean(x)
    std_dev = np.std(x)
    plt.hist(x, bins=20, density=True, alpha=0.6, color='g')
    # fitted
    xmin, xmax = plt.xlim()
    x_axis = np.linspace(xmin, xmax, 100)
    y = stats.norm.pdf(x_axis, mean, std_dev)
    plt.plot(x_axis, y, 'r--', label='Normal Distribution')
    plt.title('Histogram of Delegation size with Fitted Normal Distribution')
    plt.legend()
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_hist_fitted.png')
    else:
        plt.show()


def qq_plot(_df, year, season, _save=False, _col='delegation_size'):
    """
    Diagramme quantile-quantile :
    On calcule X quantiles
    Wikipedia :
    On choisit une distribution à vérifier (ici distribution normale)
    Si la série statistique suit bien la distribution théorique choisie,
    on devrait avoir les quantiles x i x_{i} observés égaux
    aux quantiles x i ∗ {\displaystyle x_{i}^{*}} associés au modèle théorique.
    :param _df:
    :param season:
    :param year:
    :param _col:
    :param _save:
    :return:
    """
    stats.probplot(_df[_col], dist="norm", plot=plt)
    plt.title('QQ plot')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_qq.png')
    else:
        plt.show()


def sns_gauss(_df, year, season, _save=False, _col='delegation_size'):
    sns.distplot(_df[_col], bins=30)
    plt.title('Gaussian distribution')
    if _save:
        plt.savefig(f'{SAVEFIG_DIR}/{season}_{year}/delegation_size_{season.lower()}_{year}_sns_distribution.png')
    else:
        plt.show()
