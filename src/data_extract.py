import pandas as pd
from tabulate import tabulate


########################################################################################################################
# CSV FILES & DATASET
########################################################################################################################
def create_random_sub_dataset(csv_file, n):
    f = f'../data/{csv_file}'
    df = pd.read_csv(f)
    n = min(n, len(df))
    random_sub_dataset = df.sample(n=n, random_state=164161)
    return random_sub_dataset


def create_sub_dataset_filter(csv_file, *args):
    """
    For machine learning debug purpose
    :param csv_file:
    :param args:
    :return:
    """
    noc_filter = ['FRA']
    if not args:
        noc_filter = ['USA', 'FRA', 'FRG', 'GER', 'BRA']
        noc_filter = ['USA', 'GBR', 'FRA', 'ITA', 'GER', 'CAN', 'SWE', 'JPN', 'AUS', 'HUN', 'POL', 'NED', 'SUI', 'URS',
                      'FIN', 'ESP', 'CHN', 'RUS', 'AUT', 'NOR']
    f = f'../data/{csv_file}'
    df = pd.read_csv(f)
    df.dropna(inplace=True)
    # new_df = df[(df['Year'] == 2012) & (df['NOC'].isin(noc_filter))]
    new_df = df[(df['NOC'].isin(noc_filter))]
    print_dataset(new_df, title='ML DF')
    new_df.to_csv(f'ml_{len(noc_filter)}_noc.csv', index=False)
    return df


#
# def dataset_noc_filter(csv_file):
#     twenty_noc = ['USA', 'GBR', 'FRA', 'ITA', 'GER', 'CAN', 'SWE', 'JPN', 'AUS', 'HUN', 'POL', 'NED', 'SUI', 'URS',
#                   'FIN', 'ESP', 'CHN', 'RUS', 'AUT', 'NOR']
#
#     f = f'../data/{csv_file}'
#     df = pd.read_csv(f)
#     df.dropna(inplace=True)
#     new_df = df[(df['NOC'] == 2012) & (df['NOC'].isin(twenty_noc))]
#     print_dataset(new_df, title='ML DF')
#     new_df.to_csv('ml_4_noc.csv', index=False)


def create_sub_dataset_with_x_first_values(csv_file, x):
    f = f'../data/{csv_file}'
    df = pd.read_csv(f)
    sub_dataset = df.head(x)
    return sub_dataset


def duplicated_values(df, col):
    ids = df[col]
    return df[ids.isin(ids[ids.duplicated()])].sort_values(col)


########################################################################################################################
# DATA ANALYSIS
########################################################################################################################
def print_dataset(_df, title='', _sample=True, nb=None):
    if not nb:
        nb = 10
    num_rows, num_cols = _df.shape
    truncated = 'truncated' if _sample and nb else 'full'
    print(f'************** {title} ({num_rows} rows | {num_cols} cols | {truncated})')
    if _sample:
        print(tabulate(_df[0:nb], headers='keys', tablefmt='psql'))
    else:
        print(tabulate(_df, headers='keys', tablefmt='psql'))


def print_stats_dict(_dict, _title):
    """
    huho
    """
    print(f'----- {_title} -----')
    # pprint.pprint(_dict)
    for k, v in _dict.items():
        print(f'{k} : {v}')


def __advanced_stats(_dataframe):
    _nb_col_by_type = nb_col_by_type(_dataframe=_dataframe)
    _col_name_by_type = col_name_by_type(_dataframe=_dataframe, __nb_col_by_type=_nb_col_by_type)
    unique_values = unique_values_by_col(_dataframe=_dataframe, __col_name_by_type=_col_name_by_type)

    return {
        'type': _nb_col_by_type,
        'name': _col_name_by_type,
        'unique': unique_values,
    }


def nb_col_for_type(_dataframe, _type):
    """
    Return the number of columns for one given type
    """
    s = 0
    for col in _dataframe.columns:
        s += (_type == _dataframe[col].dtype)
    return s


def all_col_of_one_type(_dataframe, _type):
    """
    Return all columns of a given type as a sub dataframe
    """
    return _dataframe.select_dtypes(include=_type)


def nb_col_by_type(_dataframe):
    """
    Return dict such as :

    bool : 1
    float64 : 2
    object : 19
    int64 : 8

    :param _dataframe:
    :return:
    """
    unic_col_types = set(_dataframe.dtypes)
    __nb_col_by_type = {
        f'{_type}': nb_col_for_type(_dataframe, _type)
        for _type in unic_col_types
    }
    # logging.debug(f'nb_col_by_type : {type(__nb_col_by_type)}')
    return __nb_col_by_type


def col_name_by_type(_dataframe, __nb_col_by_type):
    """
    Return dict such as :

    bool : ['Win']
    float64 : ['Height']
    object : ['Name', 'Team']
    int64 : ['Age']

    :param _dataframe:
    :param __nb_col_by_type:
    :return:
    """
    cols_by_type = {
        f'{t}': [c for c in all_col_of_one_type(_dataframe, t)]
        for t, _ in __nb_col_by_type.items()
    }
    # logging.debug(f'col_name_by_type : {type(cols_by_type)}')
    return cols_by_type


def unique_values_by_col(_dataframe, __col_name_by_type, only_type='object', max_value=20):
    """
    Return dict such as :

    Name : 134732
    Sex : ['M', 'F']
    Season : ['Summer', 'Winter']
    Medal : ['nan', 'Gold', 'Bronze', 'Silver']

    # Helps us find features to use One Hot Encode on
    :param _dataframe:
    :param __col_name_by_type:
    :param only_type:
    :param max_value:
    :return:
    """
    unique_values = {
        f'{c}': [
            list((_dataframe[c].unique())) if len(_dataframe[c].unique()) < max_value else len(_dataframe[c].unique())]
        for c in __col_name_by_type.get(only_type)
    }
    # logging.debug(f'unique values by col : {type(unique_values)}')
    # print('******************************************************')
    # print(f'unique values by col : {unique_values}')
    # print('******************************************************')
    return unique_values


########################################################################################################################
# EXECUTION
########################################################################################################################
def print_advanced_stats(_dataframe):
    """
    ...
    """
    print('***** STATS *****')

    # Basics
    num_rows, num_cols = _dataframe.shape
    print(f"{num_rows} Rows, {num_cols} Cols ({list(_dataframe.columns)}")
    print_dataset(_df=_dataframe, title='Olympics dataset sample', _sample=True)

    # Number of column by type
    _nb_col_by_type = nb_col_by_type(_dataframe=_dataframe)
    print_stats_dict(_dict=_nb_col_by_type, _title='Nb col by type')

    # Column name by type
    _col_name_by_type = col_name_by_type(_dataframe=_dataframe, __nb_col_by_type=_nb_col_by_type)
    print_stats_dict(_dict=_col_name_by_type, _title='Cols name by type')

    # Unique values by column (object type only & 20 max by default)
    unique_values = unique_values_by_col(_dataframe=_dataframe, __col_name_by_type=_col_name_by_type)
    a = [
        c for c in _dataframe.columns if c not in _col_name_by_type.get('object')
    ]
    print_stats_dict(_dict=unique_values, _title='Unique values for object type column (limit 20)')
    print(f'Ignored col for unique values : {a}')

    cities = _dataframe['City'].unique()
    print(f'Unique cities : {cities}')
