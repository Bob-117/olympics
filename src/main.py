import pandas as pd
from data_extract import print_advanced_stats, create_random_sub_dataset, create_sub_dataset_filter
from data_tranform import one_hot_encode, nan_process

from data_visu import plot
from machine_learning import ml
from regions import regions_process

ATHLETE_CSV = 'athlete_events.csv'
SUB_ATHLETE_CSV = '10000_athlete.csv'
ML_SAMPLE = 'ml_20_noc.csv'


def sub_dataset():
    n = 10_000
    n_athlete_d = create_random_sub_dataset(ATHLETE_CSV, n)
    print(len(n_athlete_d))
    n_athlete_d.to_csv(f'../data/{n}_athlete.csv', index=False)


def main_process():
    """
    Main process, uncomment what u want to run
    :return:
    """

    # DATA
    # ## Create sub dataset
    sub_dataset()
    create_sub_dataset_filter(csv_file='../data/athlete_events.csv')

    # ## Load data
    athletes = pd.read_csv(f'../data/{ATHLETE_CSV}', dtype='unicode')

    regions = pd.read_csv('../data/noc_regions.csv')

    # ## Analysis
    print_advanced_stats(athletes)

    # ## Clean
    athletes = nan_process(athletes)

    # ## Regions
    regions_process(athletes=athletes, regions=regions)

    # ## Visualisation
    plot(athletes, save=False)

    # Machine Learning
    # ## Sub dataset performance and development purpose
    create_sub_dataset_filter(SUB_ATHLETE_CSV)

    # ## Models
    ml(athletes)


if __name__ == '__main__':
    main_process()
