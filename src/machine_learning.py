import pprint

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, ConfusionMatrixDisplay, roc_curve, \
    auc
from sklearn.model_selection import train_test_split, StratifiedKFold, cross_val_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

from data_extract import print_dataset

########################################################################################################################
# DATA
########################################################################################################################

noc_continent = {
    "Africa": ["DJI", "EGY", "ALG", "CMR", "SEN", "NGR", "SUD", "MAR", "TUN", "KEN", "GUI", "RSA", "LIB", "UGA", "ETH",
               "GHA", "COD", "ZIM", "ANG", "CIV", "TAN", "LES", "SWZ", "SLE", "MLI", "NAM", "BDI", "SOM", "COM", "STP",
               "GAB", "GMB", "CPV", "BFA", "MTN", "SEY", "ERI"],
    "Asia": ["HKG", "KOR", "CHN", "KSA", "AZE", "KAZ", "PAK", "TJK", "UZB", "KGZ", "GEO", "INA", "BRN", "BHU", "TLS",
             "JOR", "TKM", "COM", "STP", "PLW", "IVB", "BDI", "HAI", "CGO", "SAM", "MAL", "ASA", "MDV", "GUM", "AHO",
             "SSD", "IOA", "CHA", "EUN", "YAR", "YMD"],
    "Europe": ["POL", "FRA", "EST", "AUT", "SWE", "RUS", "FRG", "UKR", "CYP", "HUN", "SUI", "TCH", "ITA", "LUX", "DEN",
               "TUR", "FIN", "CZE", "GER", "SLO", "BEL", "SCG", "BUL", "NED", "SVK", "NOR", "BLR", "POR", "LAT", "VIE",
               "IRL", "LTU", "SMR", "MNE", "CRO", "ISR", "DOM", "TTO", "MDA", "BIH", "VEN", "SRB", "CRC", "BER", "LIE",
               "MON", "ISL", "AND", "GIB", "MLT", "MKD", "COK", "GDR", "WIF"],
    "North America": ["PUR", "USA", "CAN", "MEX", "CUB", "BAH", "JAM", "DOM", "TTO", "CRC", "BER", "GUM", "AHO", "ESA",
                      "PAN", "NBO", "ARU", "NRU"],
    "Oceania": ["AUS", "NZL", "FIJ", "ASA", "GUM", "AHO", "COK"],
    "South America": ["ARG", "BRA", "COL", "PER", "ECU", "VEN", "URU", "CHI", "PAR", "BOL", "GUY", "SUR"]
}

cities_continent = {
    "Asia": ["Seoul", "Tokyo", "Nagano", "Sapporo", "Beijing"],
    "Europe": ["Paris", "Antwerpen", "Athina", "Cortina d'Ampezzo", "Helsinki", "Stockholm", "Oslo", "Berlin",
               "Sarajevo", "Roma", "Grenoble", "Innsbruck", "Amsterdam", "Lillehammer", "Moscow", "Sochi", "Munich",
               "Barcelona", "London", "St. Moritz", "Garmisch-Partenkirchen", "Albertville", "Torino", "Chamonix"],
    "North America": ["Montreal", "St. Louis", "Mexico City", "Calgary", "Los Angeles", "Lake Placid", "Atlanta",
                      "Salt Lake City", "Vancouver", "Squaw Valley"],
    "Oceania": ["Melbourne", "Sydney"]
}


########################################################################################################################
# EXECUTION
########################################################################################################################
def ml(df):
    print('*******************************')
    print('************** ML  ************')
    print('*******************************')
    svm_countries_two_medals_two_discipline(data=df)
    knn_athlete_two_medals(data=df)


def prepare_data(_df, _features, _target, _size=.2):
    # x = _df.drop(_features, axis=1)
    x = _df[_features]
    y = _df[_target]
    print(f'Dataset size : {len(y)}')
    # rescale
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=_size, random_state=42)
    return x_train, x_test, y_train, y_test


########################################################################################################################
# SVM
########################################################################################################################
def svm_countries_two_medals_two_discipline(data):
    """
    Pouvez-vous prédire les pays médaillés dans au moins 2 disciplines lors des JO de Paris ?
    :param data:
    :return:
    """
    print('Features selection for SVM')
    features_df = __svm_features_selection(df=data)
    # features_df = pd.read_csv('../data/ml_dataset_svm_all.csv')
    print('Training')
    __svm_process(features_df)


def __svm_features_selection(df):
    """
    feature selection for SVM training
    Target : bool(country already won two medals in two discipline in one event)
    Features : delegation size, distance country/city, total medal until the current event
    :param df:
    :return:
    """
    print(f'Selection de features SVM a partir de {df.columns}')
    print(f"Selection des colonnes ['NOC', 'Year', 'Season', 'City', 'Medal', 'Sport', 'Name']")
    print(f'Cities : {df["City"].unique()}')
    print(f'NOC : {df["NOC"].unique()}')
    print(f'MEDAL : {df["Medal"].unique()}')
    print(f'YEAR : {df["Year"].unique()}')
    countries_informations = df[['NOC', 'Year', 'Season', 'City', 'Medal', 'Sport', 'Name']]
    print_dataset(countries_informations, 'raw information for svm')
    print('Gestion des médailles')
    print('One Hot Encode')
    columns_to_encode = ['Medal']
    encoded_medal = pd.get_dummies(countries_informations, columns=columns_to_encode, dtype=int)
    print_dataset(encoded_medal, title='Encoded medals')

    # print(noc_continent)
    # print(cities_continent)
    # print(encoded_medal[(encoded_medal['Medal_Gold'] == 1)])

    # delegation_df = __delegation_feature(df)
    # print_dataset(delegation_df, title='delegation size for SVM feature')

    features_df = encoded_medal.copy()
    noc = features_df['NOC'].unique()
    out = []

    for _id, country in enumerate(noc):
        print(f'\n========== Country : {country} {_id} / {len(noc)}')
        total_medals_in_history = 0
        for year in features_df['Year'].unique():
            for season in features_df['Season'].unique():

                """
                ONE SPECIFIC EVENT
                """
                specific_event = features_df[(features_df['NOC'] == country) & (features_df['Year'] == year) & (
                        features_df['Season'] == season)]

                # todo remove
                if country == 'FRA':
                    print_dataset(specific_event, title=f'FRA {season} {year}')

                if len(specific_event) > 0:
                    """
                    FEATURE : Count medals 2 for one sport                   
                    """
                    medals_count_by_sport = specific_event[
                        ['Sport', 'Medal_Bronze', 'Medal_Silver', 'Medal_Gold']
                    ].groupby('Sport').sum()
                    sports_with_at_least_two_medals = medals_count_by_sport[
                        (medals_count_by_sport[['Medal_Bronze', 'Medal_Silver', 'Medal_Gold']] > 0).sum(axis=1) >= 2
                        ]
                    ########
                    # medals_count_by_sport = specific_event.groupby('Sport').agg({
                    #     'Medal_Bronze': 'sum',
                    #     'Medal_Silver': 'sum',
                    #     'Medal_Gold': 'sum'
                    # })
                    # sports_with_at_least_two_medals = medals_count_by_sport[
                    #     (medals_count_by_sport[['Medal_Bronze', 'Medal_Silver', 'Medal_Gold']].gt(0)).sum(axis=1) >= 2
                    #     ]
                    # result = sports_with_at_least_two_medals.index.tolist()
                    # print('__________________________  ', result)
                    ########
                    if country == 'FRA':
                        print_dataset(sports_with_at_least_two_medals, title='sports_with_at_least_two_medals')
                        print_dataset(medals_count_by_sport, title='medals_count_by_sport')
                    # Debug 2 medals
                    # if not sports_with_at_least_two_medals.empty:
                    #     _sports = sports_with_at_least_two_medals.index.values
                    #     if country == 'FRA':
                    #         print(
                    #             f'Country {country} has at least 1 medal in 2 sports ({season} {year}) : {_sports}')
                    #         print_dataset(sports_with_at_least_two_medals, _sample=False)
                    # else:
                    #     if country == 'FRA':
                    #         print(f'{country} does not have at least one medal in 2 sports for event {season} {year}')

                    # two_medal_target = not sports_with_at_least_two_medals.empty

                    """
                    FEATURE : Count medals in at least 2 sports                 
                    """
                    sports_with_medals = specific_event[
                        specific_event[['Medal_Bronze', 'Medal_Silver', 'Medal_Gold']].sum(axis=1) > 0
                        ]['Sport'].unique()
                    if country == 'FRA':
                        if len(sports_with_medals) >= 2:
                            print(
                                f'There are at least two different sports with medals {country} {season} {year} : {sports_with_medals}')
                        else:
                            print(f'There are not enough different sports with medals {country} {season} {year}.')
                    two_medal_target = len(sports_with_medals) >= 2

                    """
                    FEATURE :  same continent or not (replace by distance ?)
                    """
                    current_city = list(specific_event['City'].unique())

                    def is_in_continent(_noc_code, _city=''):
                        for _continent, _noc in noc_continent.items():
                            if _noc_code in _noc and _city in cities_continent.get(_continent, []):
                                if country == 'FRA':
                                    print(f'{_noc_code} and {_city} are in {_continent}')
                                return True
                            if country == 'FRA':
                                print(f'{_noc_code} and {_city} are not both in {_continent}')
                        return False

                    in_continent = is_in_continent(country, current_city[0])
                    # Debug : 2 cities summer 1956
                    # if len(current_city) > 1:
                    #     print('====> ', current_city, season, year, noc, country)
                    # if season == 'Summer' and year == '1956':
                    #     print_dataset(specific_event, _sample=False, title='2 cities summer 1956')
                    # NOC SAA => does not exist ?
                    """
                    FEATURE : delegation size
                    """
                    delegation = len(specific_event['Name'].unique())

                    """
                    FEATURE : total medals until now
                    """
                    s_e = specific_event
                    medal_this_event = s_e['Medal_Bronze'].sum() + s_e['Medal_Silver'].sum() + s_e['Medal_Gold'].sum()
                    total_medals_in_history += medal_this_event

                    """
                    RESULT
                    """
                    res = ({
                        'has_won_more_that_2': two_medal_target,
                        'same_continent': in_continent,
                        'delegation_size': delegation,
                        'total_medal_til_today': total_medals_in_history,
                        'metadata': f'{country}-{season}-{year}-{current_city[0]}',
                    })
                    out.append(res)

    ml_dataset = pd.DataFrame(out)
    print_dataset(ml_dataset, title='ml_dataset')
    a = input('save ?')
    if a == 'yes':
        ml_dataset.to_csv('../data/ml_dataset_svm_all.csv', index=False)
    return ml_dataset

    # delegation_df = __delegation_feature(df)
    # merged_df = pd.merge(features_df, delegation_df, on=['NOC'], how='inner')
    # print_dataset(merged_df, title='merge 1')
    # merged_df['at_least_2_disciplines'] = merged_df[['Medal_Bronze', 'Medal_Gold', 'Medal_Silver']].sum(axis=1) >= 2
    # print_dataset(merged_df, title='merged  2')
    # X = merged_df[['delegation_size', 'Season_Summer']]
    # y = merged_df['at_least_2_disciplines']


def __delegation_feature(df):
    df = df[['NOC', 'Year', 'Season_Summer']]
    delegation_count_df = df.groupby(['NOC', 'Year', 'Season_Summer']).size().reset_index(name='delegation_size')
    return delegation_count_df


def __svm_process(_data):
    selected_features = ['same_continent', 'delegation_size', 'total_medal_til_today']
    selected_target = 'has_won_more_that_2'
    x_train, x_test, y_train, y_test = prepare_data(_df=_data, _features=selected_features, _target=selected_target)

    # Standardize
    # scaler = StandardScaler()
    # X_train_scaled = scaler.fit_transform(X_train)
    # X_test_scaled = scaler.transform(X_test)

    # Model
    svm_model = SVC(kernel='linear', C=1.0, random_state=42)

    # Cross-validation
    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=42)  # You can adjust the number of splits as needed
    cv_scores = cross_val_score(svm_model, x_train, y_train, cv=cv, scoring='accuracy')

    # Train
    svm_model.fit(x_train, y_train)

    # Prediction test
    y_pred = svm_model.predict(x_test)

    accuracy = accuracy_score(y_test, y_pred)
    conf_matrix = confusion_matrix(y_test, y_pred)

    disp = ConfusionMatrixDisplay(confusion_matrix=conf_matrix)
    disp = disp.plot(cmap=plt.cm.Blues, values_format='g')
    plt.show()

    classification_rep = classification_report(y_test, y_pred, zero_division=1)  # todo

    print(f'Accuracy: {accuracy}')
    print(f'Confusion Matrix:\n{conf_matrix}')
    print(f'Classification Report:\n{classification_rep}')

    # Display cross-validation results
    print(f'Cross-Validation Scores: {cv_scores}')
    print(f'Mean Accuracy: {cv_scores.mean()}')
    print(f'Standard Deviation: {cv_scores.std()}')

    # new_athlete = np.random.rand(len(x_train.columns)).reshape(1, -1)
    new_data = [
        [True, 100, 0],
        [True, 10000, 0],
        [True, 500, 100],
        [False, 100, 0],
        [False, 10000, 0],
        [False, 1, 100000],
    ]

    for data in new_data:
        new = {
            'same_continent': data[0],
            'delegation_size': data[1],
            'total_medal_til_today': data[2]
        }
        new_data = np.array([[new['same_continent'], new['delegation_size'], new['total_medal_til_today']]])

        prediction = svm_model.predict(new_data)

        print(f'Input : {new}\nOutput: will win at least 1 medal in two separate sports : {prediction}')

    decision_scores = svm_model.decision_function(x_test)
    print(f'Decision Scores for the test set:\n{decision_scores}')

    feature_importance = svm_model.coef_[0]
    feature_importance_df = pd.DataFrame({'Feature': x_train.columns, 'Importance': feature_importance})
    print(feature_importance_df)
