import os

import ydata_profiling
import pandas as pd
import argparse


if __name__ == '__main__':
    PROJECT_DIR = os.path.dirname(os.path.abspath(__file__)).split('src')[0]
    argparse = argparse.ArgumentParser()
    argparse.add_argument('--input', type=str,
                          default=os.path.join(PROJECT_DIR, 'data', 'athlete_events.csv'))
    argparse.add_argument('--output', type=str,
                          default=os.path.join(PROJECT_DIR, 'data', 'athlete_events_report.html'))
    args = argparse.parse_args()

    df = pd.read_csv(args.input)
    profile = ydata_profiling.ProfileReport(df)
    profile.to_file(args.output)
