import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

# Charger le dataset depuis le fichier CSV
data = pd.read_csv("chemin/vers/ton/dataset.csv")

# Question 1 : Combien d’athlètes français ont été médaillés dans l’histoire des JO ?
athletes_francais_medailles = data[(data['Team'] == 'France') & (data['Medal'].notnull())]
nombre_athletes_francais_medailles = len(athletes_francais_medailles)
print(f"Nombre d'athlètes français médaillés : {nombre_athletes_francais_medailles}")

# Question 2 : Quels sont les plus grands athlètes français dans l’histoire des JO ?
# Tu peux utiliser divers critères comme le nombre total de médailles ou le type de médailles remportées.

# Question 3 : Comment se classe la France dans l’histoire des JO ?
# Calculer le nombre total de médailles remportées par la France.

# Question 4 : Quels sont les pays qui ont le plus gagné par discipline ?
pays_plus_gagnants_par_discipline = data.groupby(['Sport', 'Team'])['Medal'].count().reset_index()
pays_plus_gagnants_par_discipline = pays_plus_gagnants_par_discipline.sort_values(by='Medal', ascending=False)
print(pays_plus_gagnants_par_discipline)

# Question 5 : Quels sont les athlètes qui ont le plus gagné de médailles dans l’histoire des JO ?
athletes_plus_medailles = data.groupby('Name')['Medal'].count().reset_index()
athletes_plus_medailles = athletes_plus_medailles.sort_values(by='Medal', ascending=False)
print(athletes_plus_medailles)

# Diviser le dataset pour la prédiction
# (Assure-toi que ton dataset contient des données pour les JO de Paris)
train_data = data[data['Year'] < 2024]
test_data = data[data['Year'] == 2024]

# Question 6, 7, 8 : Utilisation de l'apprentissage automatique pour les prédictions
features = ['Age', 'Height', 'Weight']  # Choisis les caractéristiques pertinentes pour la prédiction
target = 'Medal'  # La variable cible que nous voulons prédire

X_train, X_test, y_train, y_test = train_test_split(train_data[features], train_data[target], test_size=0.2, random_state=42)

# Utilisation d'un modèle simple de forêt aléatoire pour l'exemple
model = RandomForestClassifier()
model.fit(X_train, y_train)

# Prédiction sur les données de test
predictions = model.predict(X_test)

# Évaluation de la précision du modèle
accuracy = accuracy_score(y_test, predictions)
print(f"Précision du modèle : {accuracy}")

# Tu peux maintenant utiliser le modèle pour répondre aux questions 6, 7, 8 avec les données de test.
