from nicegui import app, ui

import login
import olympic_stats
from login import AuthMiddleware

app.add_middleware(AuthMiddleware)
app.include_router(login.router)
app.include_router(olympic_stats.router)

ui.run(host='127.0.0.1', port=5001, title='Olympic Games', storage_secret='THIS_NEEDS_TO_BE_CHANGED', dark=True)
