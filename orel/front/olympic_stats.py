from nicegui import ui, app, APIRouter
from nicegui.functions.refreshable import refreshable
from login import AuthMiddleware
import pandas as pd

# DATA
df = pd.read_csv('../data/athlete_events.csv')
rows = df.to_dict(orient='records')
max_rows = 280000

# CONSTANTS
YEARS = ["Tous", "2021", "2016", "2012", "2008", "2004", "2000", "1996", "1992", "1988", "1984", "1980", "1976", "1972",
         "1968", "1964", "1960", "1956", "1952", "1948", "1936", "1932", "1928", "1924", "1920", "1912", "1908", "1904",
         "1900", "1896"]
COUNTRIES = ["Monde"] + list(df['Team'].unique())
MEDALS = ["Toutes", "Gold", "Silver", "Bronze"]
COLUMNS = [
    {'name': 'id', 'label': 'ID', 'field': 'ID', 'required': True, 'align': 'left'},
    {'name': 'name', 'label': 'Name', 'field': 'Name', 'required': True},
    {'name': 'sex', 'label': 'Sex', 'field': 'Sex', 'sortable': True},
    {'name': 'age', 'label': 'Age', 'field': 'Age', 'type': 'number'},
    {'name': 'height', 'label': 'Height', 'field': 'Height', 'type': 'number'},
    {'name': 'weight', 'label': 'Weight', 'field': 'Weight', 'type': 'number'},
    {'name': 'team', 'label': 'Team', 'field': 'Team'},
    {'name': 'noc', 'label': 'Noc', 'field': 'NOC'},
    {'name': 'games', 'label': 'Games', 'field': 'Games'},
    {'name': 'year', 'label': 'Year', 'field': 'Year', 'type': 'number'},
    {'name': 'season', 'label': 'Season', 'field': 'Season'},
    {'name': 'city', 'label': 'City', 'field': 'City'},
    {'name': 'sport', 'label': 'Sport', 'field': 'Sport'},
    {'name': 'event', 'label': 'Event', 'field': 'Event'},
    {'name': 'medal', 'label': 'Medal', 'field': 'Medal'},
]

# UI
# Tailwind CSS classes
t_country_selector_classes = "appearance-none border border-red-300 hover:border-red-500 rounded-md p-2"
t_year_selector_classes = "appearance-none border border-blue-300 hover:border-blue-500 rounded-md p-2"
t_medal_selector_classes = "appearance-none border border-green-300 hover:border-green-500 rounded-md p-2"

# ROUTER
router = APIRouter(prefix='/olympic')


# PAGE
@router.page('/stats')
def olympic_stats() -> None:
    # Menu header
    result = ui.label().classes('mr-auto')
    with ui.row().classes('w-full items-center'):
        ui.label('Statistiques sur les JO').classes('text-4xl font-bold')
        result = ui.label().classes('mr-auto')
        with ui.button(icon='menu'):
            with ui.menu() as menu:
                with ui.link(target='http://localhost:5001/olympic/stats'):
                    ui.menu_item('Statistiques sur les JO')
                ui.separator()
                with ui.link(target='http://localhost:5001/auth/login'):
                    ui.menu_item('Connexion')
                ui.separator()
                ui.menu_item('Fermer', on_click=menu.close)
    ui.separator()

    # French medals chart
    ui.label('Nombre de médailles françaises par année et type').classes('text-2xl')
    _chart = create_chart(df, 'France')
    with ui.select(COUNTRIES, label="PAYS", value=COUNTRIES[0]) as country_selector_graph:
        country_selector_graph.on('change', lambda country=country_selector_graph: create_chart.refresh(df, country))
    ui.separator()

    # Filters and search in dataframe
    ui.label('Filtres').classes('text-2xl')
    with ui.row().classes('items-center').style('width: 100%'):
        _country_selector = ui.select(YEARS, label="ANNÉE", value=YEARS[0]).classes(t_country_selector_classes).style(
            'width: 300px')
        year_selector = ui.select(COUNTRIES, label="PAYS", value=COUNTRIES[0]).classes(t_year_selector_classes).style(
            'width: 300px')
        medal_selector = ui.select(MEDALS, label="MÉDAILLE", value=MEDALS[0]).classes(t_medal_selector_classes).style(
            'width: 300px')
    with ui.row().classes('items-center').style('width: 100%'):
        ui.button('Chercher', on_click=lambda: filter_results(_table, _country_selector.value, year_selector.value,
                                                              medal_selector.value), color='green')
        ui.button('Supprimer une ligne', on_click=lambda: delete_row(_table), color='red')

    # Table
    _table = create_table()


def create_table():
    return ui.table(columns=COLUMNS, rows=rows[:max_rows], title="Résultats", row_key='Name', pagination=50)


@refreshable
def create_chart(new_df, _country):
    if not isinstance(_country, str):
        _country = _country.value
        print(_country)
    if _country == "Tous":
        _country = "France"
    new_df['Medal'] = new_df['Medal'].fillna('No Medal')
    france_df = new_df[(new_df['Team'] == _country) & (new_df['Medal'].isin(['Gold', 'Silver', 'Bronze', 'No Medal']))]
    medals_by_year = france_df.groupby(['Year', 'Medal']).size().unstack(fill_value=0)
    filtered_medals = ['Gold', 'Silver', 'Bronze']
    medals_by_year = medals_by_year[filtered_medals]
    gold_data = medals_by_year.get('Gold', []).tolist()
    silver_data = medals_by_year.get('Silver', []).tolist()
    bronze_data = medals_by_year.get('Bronze', []).tolist()

    echart = ui.echart({
        'xAxis': {'type': 'category', 'data': list(medals_by_year.index)},
        'yAxis': {'type': 'value'},
        'legend': {'data': ['Gold', 'Silver', 'Bronze']},
        'color': ['#FFD700', '#C0C0C0', '#8B4513'],  # Couleurs pour les médailles Gold, Silver et Bronze respectivement
        'series': [
            {'name': 'Gold', 'type': 'bar', 'stack': 'medals', 'barMaxWidth': 30, 'data': gold_data},
            {'name': 'Silver', 'type': 'bar', 'stack': 'medals', 'barMaxWidth': 30, 'data': silver_data},
            {'name': 'Bronze', 'type': 'bar', 'stack': 'medals', 'barMaxWidth': 30, 'data': bronze_data},
        ],
    })
    return echart


def filter_results(table, selected_year, selected_country, selected_medal):
    filtered_rows = rows.copy()
    if selected_year != "Tous":
        filtered_rows = [row for row in filtered_rows if str(row['Year']) == selected_year]
    if selected_country != "Monde":
        filtered_rows = [row for row in filtered_rows if row['Team'] == selected_country]
    if selected_medal != "Toutes":
        filtered_rows = [row for row in filtered_rows if row['Medal'] == selected_medal]
    table.rows = filtered_rows[:max_rows]


# Development function
def sort_result(table):
    rows.append(
        {'Name': 'AppendBob', 'Sex': 'M', 'Age': 21, 'Height': 1.80, 'Weight': 80, 'Team': 'France', 'NOC': 'FRA',
         'Games': 'Paris 2024', 'Year': 2024, 'Season': 'Summer', 'City': 'Paris', 'Sport': 'Judo',
         'Event': 'Judo Men\'s Extra-Lightweight', 'Medal': 'Gold', 'Gold': 1})
    ui.notify('Request launched !')
    table.update()


def delete_row(table):
    if len(rows) > 0:
        rows.pop()
        table.update()
    else:
        ui.notify("No more rows to delete")
